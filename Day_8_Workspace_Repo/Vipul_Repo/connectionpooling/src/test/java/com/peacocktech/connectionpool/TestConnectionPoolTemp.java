package com.peacocktech.connectionpool;

import java.io.IOException;

import org.junit.Test;

import com.peacocktech.connectionpool.ConnectionPool;

/**
 * @author Dipak
 * @author Gautam
 * @author Shweta
 * @author Vipul
 *
 */
public class TestConnectionPoolTemp {
	ConnectionPool connectionPool = ConnectionPool.getInstance();
	/*
	 *
	 * -------- CASE-1 THREAD_POOL-10000, POOL_SIZE=51, WAIT_TIME=5000\n
	 * -------- CASE-2 THREAD_POOL-50000, POOL_SIZE=60, WAIT_TIME=10000\n
	 * 
	 */

	// in it thread size with 1000 thread
	int threadSize = 50000;
	// init pool size with 10
	int poolSize = 60;
	// init wait time with 1s
	int waitTime = 10000;

	/**
	 * calculate milis to sec
	 * 
	 * @param startTime
	 * @return
	 */
	public double countelaspedTime(long startTime) {
		long now = System.currentTimeMillis();
		return (now - startTime) / 1000.0;
	}

	/**
	 * 1st thread size, pool size and wait time to test for different case
	 */
	@Test
	public void testInsertDataThread() {
		try {
			DataSource.getInstance().setConfiguration(poolSize, waitTime);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		connectionPool.insertData("root", 1, 10000);
		long startTime = System.currentTimeMillis();
		connectionPool.threadinsert(threadSize);
		System.out.println(countelaspedTime(startTime));
	}
	//
	// @Test
	// public void testinsertdatathread500() {
	// connectionPool.insertData("root", 1, 10000);
	// long start = System.currentTimeMillis();
	// connectionPool.threadinsert(500);
	// System.out.println(countelaspedTime(start));
	//
	// }
	//
	// @Test
	// public void testinsertdatathread1000() throws IOException {
	// connectionPool.insertData("root", 1, 10000);
	// long start = System.currentTimeMillis();
	// connectionPool.threadinsert(1000);
	// System.out.println(countelaspedTime(start));
	//
	// }
	//
	// @Test
	// public void testinsertdatathread2000() throws IOException {
	// DataSource.getInstance().setconfiguration(10, 2000);
	// connectionPool.insertData("root", 1, 10000);
	// long start = System.currentTimeMillis();
	// connectionPool.threadinsert(2000);
	// System.out.println(countelaspedTime(start));
	//
	// }
	//
	// @Test
	// public void testinsertdatathread3000() {
	// connectionPool.insertData("root", 1, 10000);
	// long start = System.currentTimeMillis();
	// connectionPool.threadinsert(3000);
	// System.out.println(countelaspedTime(start));
	//
	// }
	//
	// // @Test
	// // public void testinsertdatathread5000() {
	// //
	// // connectionpool.insertdata("insert into
	// // Employee(empName,departId,salary)values('ddd',5,3444)");
	// // long start = System.currentTimeMillis();
	// // connectionpool.threadinsert(5000);
	// // System.out.println(countelaspedTime(start));
	// //
	// // }
	//
	// @Test
	// public void testinsertthread1000() throws IOException {
	// DataSource.getInstance().setconfiguration(10, 2000);
	// connectionPool.insertData("root", 1, 10000);
	// long start = System.currentTimeMillis();
	// connectionPool.threadinsert(1000);
	// System.out.println(countelaspedTime(start));
	//
	// }
	//
	// @Test
	// public void testinsertthread2000() throws IOException {
	// DataSource.getInstance().setconfiguration(10, 2000);
	// connectionPool.insertData("root", 1, 10000);
	// long start = System.currentTimeMillis();
	// connectionPool.threadinsert(2000);
	// System.out.println(countelaspedTime(start));
	//
	// }
	//
	// @Test
	// public void testinsertthread3000() throws IOException {
	// DataSource.getInstance().setconfiguration(10, 2000);
	// connectionPool.insertData("root", 1, 10000);
	// long start = System.currentTimeMillis();
	// connectionPool.threadinsert(3000);
	// System.out.println(countelaspedTime(start));
	//
	// }

	// @Test
	// public void testinsertdatathread10000() {
	//
	// connectionpool.insertdata("insert into
	// Employee(empName,departId,salary)values('ddd',5,3444)");
	// long start = System.currentTimeMillis();
	// for (int i = 0; i < 1000; i++) {
	// final int threadnum = i;
	// Thread thread = new Thread(new Runnable() {
	// public void run() {
	// connectionpool.insertdata("insert into Employee(empName,departId,salary)
	// values('ddd "+threadnum+"',5,3444)");
	// }
	// },"Thread"+i);
	// thread.start();
	// }
	// System.out.println(countelaspedTime(start));
	//
	// }
	// @Test
	// public void testinsertinvalidedata()
	// {
	// assertNotEquals(0, connectionpool.insertdata("insert into Employee
	// (empName,departId,salary)values('ddd',5,4000)"));
	// }
	// @Test
	// public void testselectdata()
	// {
	// assertEquals(true, connectionpool.getdata("select * from Employee"));
	// }
	// @Test
	// public void testselectwrongquery()
	// {
	// assertNotEquals(false, connectionpool.getdata("select * from Employee
	// where salary > 3000"));
	// }
	// @Test
	// public void testselectwithwhere()
	// {
	// assertEquals(true, connectionpool.getdata("select * from Employee where
	// empName = 'shweta'"));
	// }
	// @Test
	// public void testselectwithwherenull()
	// {
	// assertEquals(true, connectionpool.getdata("select * from Employee where
	// empName = null"));
	// }
	// @Test
	// public void testwritingData3()
	// {
	// connectionpool.threadInsert(200);
	// }

}
