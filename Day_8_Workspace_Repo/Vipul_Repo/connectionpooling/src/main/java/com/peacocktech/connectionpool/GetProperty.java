package com.peacocktech.connectionpool;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Dipak
 * @author Gautam
 * @author Shweta
 * @author Vipul
 *
 */
public class GetProperty {

	private static final String IO_EXECPTION = "IO Exception occur";

	private String strUserName;
	private String strPassword;
	private String strDriver;
	private int poolSize = 10;
	private int waitTimeForThread = 1000;
	private final Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private String uri;

	/**
	 * constructor for load configuration file
	 * 
	 */
	public GetProperty() {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			String filename = "config.properties";
			input = GetProperty.class.getClassLoader().getResourceAsStream(filename);
			// load a properties file
			prop.load(input);

			// get the property value and set to variable
			uri = prop.getProperty("database");
			strUserName = prop.getProperty("dbuser");
			strPassword = prop.getProperty("dbpassword");
			strDriver = prop.getProperty("driver");
			poolSize = Integer.parseInt(prop.getProperty("pool"));
			waitTimeForThread = Integer.parseInt(prop.getProperty("waittime"));

		} catch (IOException ex) {
			log.log(Level.SEVERE, IO_EXECPTION, ex);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					log.log(Level.SEVERE, IO_EXECPTION, e);
				}
			}
		} // finally
	}// get properties

	/**
	 * getter for url
	 * 
	 * @return
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * getter for name
	 * 
	 * @return
	 */
	public String getUsername() {
		return strUserName;
	}

	/**
	 * getter for pwd
	 * 
	 * @return
	 */
	public String getPassword() {
		return strPassword;
	}

	/**
	 * getter for driver
	 * 
	 * @return
	 */
	public String getDriver() {
		return strDriver;
	}

	/**
	 * getter for pool
	 * 
	 * @return
	 */
	public int getPool() {
		return poolSize;
	}

	/**
	 * getter for wait
	 * 
	 * @return
	 */
	public int getWait() {
		return waitTimeForThread;
	}
}// class
