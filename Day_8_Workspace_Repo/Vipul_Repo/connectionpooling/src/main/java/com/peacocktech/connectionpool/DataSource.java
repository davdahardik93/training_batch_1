package com.peacocktech.connectionpool;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.dbcp2.BasicDataSource;

/**
 * @author Dipak
 * @author Gautam
 * @author Shweta
 * @author Vipul
 *
 */
public class DataSource {

	private static DataSource datasource;
	private BasicDataSource bds;
	static int count = 0;
	static final Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private DataSource() throws IOException, SQLException {
		// configuration of database connection
		bds = new BasicDataSource();
		// counter for get number of objectF
		count++;
		log.info("done obj " + count);
		GetProperty ge = new GetProperty();
		bds.setUsername(ge.getUsername());// ge.username
		bds.setPassword(ge.getPassword());//
		bds.setUrl(ge.getUri());// "jdbc:mysql://localhost:3306/Name"
		bds.setDriverClassName(ge.getDriver());// "com.mysql.jdbc.Driver"
		// below settings for dbcp can work with defaults
		bds.setInitialSize(ge.getPool());
		bds.setMaxTotal(ge.getPool());
		bds.setMaxIdle(10);
		bds.setMaxWaitMillis(ge.getWait());
		// getConnection();
	}// cons()

	/**
	 * method to get instance of data source
	 * 
	 * @return object of data source
	 * @throws IOException
	 */
	public static DataSource getInstance() throws IOException {
		if (datasource == null)
			try {
				datasource = new DataSource();
			} catch (SQLException e) {
				log.log(Level.SEVERE, "SQL exception", e);
			}
		return datasource;

	}// get instance

	/**
	 * get instance of connection
	 * 
	 * @return connection object for pool
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException {
		return bds.getConnection();
	}

	/**
	 * set pool size and wait mills for pool
	 * 
	 * @param pool
	 * @param wait
	 */
	public void setConfiguration(int pool, int wait) {
		bds.setInitialSize(pool);
		bds.setMaxTotal(pool);
		bds.setMaxWaitMillis(wait);
	}// set config
}// class
