package com.peacocktech;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * op
 * 
 * @author peacock
 *
 */
public class OperationMethod {

	Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	/**
	 * 
	 * @param name
	 */
	public void insert(String name) {
		String query = "INSERT INTO employee(emp_name) VALUES('" + name + "')";
		Connection conn = null;
		Statement stmt = null;
		try {
			conn = DBCPool.getInstance().getConnection();
			stmt = conn.createStatement();
			stmt.executeUpdate(query);

		} catch (SQLException e) {
			logger.log(Level.SEVERE, e.toString(), e);
		}
		try {
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @throws SQLException
	 */
	public void select() throws SQLException {
		logger.info("Executing statement.");
		Connection conn = DBCPool.getInstance().getConnection();
		Statement stmt = conn.createStatement();
		ResultSet rset;
		rset = stmt.executeQuery("SELECT * FROM employee");
		while (rset.next()) {
			logger.info(rset.getString("emp_name"));
		}
		rset.close();
		stmt.close();
	}

	public void insertThread(int count) {

		for (int i = 0; i < count; i++) {
			MyThreadPool p = new MyThreadPool();
			Thread t = new Thread(p);
			t.start();
		}

	}
}
