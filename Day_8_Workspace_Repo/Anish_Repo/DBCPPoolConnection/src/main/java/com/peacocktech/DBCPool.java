package com.peacocktech;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.dbcp2.BasicDataSource;

/**
 * 
 * @author peacock
 *
 */
public class DBCPool {
	static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private BasicDataSource bdp;
	private static DBCPool dbcpool;

	/**
	 * 
	 * @throws IOException
	 * @throws SQLException
	 */
	private DBCPool() throws IOException, SQLException {
		Properties prop = new Properties();
		String filename = "resource.properties";
		InputStream is = null;
		is = DBCPool.class.getClassLoader().getResourceAsStream(filename);
		prop.load(is);
		bdp = new BasicDataSource();
		bdp.setDriverClassName(prop.getProperty("driver_class"));
		bdp.setUsername(prop.getProperty("user_name"));
		bdp.setPassword(prop.getProperty("password"));
		bdp.setUrl(prop.getProperty("url"));
		Connection conn = bdp.getConnection();
		conn.close();
		logger.info("Entered Insert");
	}

	public static DBCPool getInstance() {
		if (dbcpool == null)
			try {
				dbcpool = new DBCPool();
			} catch (IOException e) {
				logger.log(Level.WARNING, e.toString());
			} catch (SQLException e) {
				logger.log(Level.SEVERE, e.toString());
			}
		return dbcpool;
	}

	public Connection getConnection() throws SQLException {
		return bdp.getConnection();
	}

	/**
	 * 
	 * @param maxtotal
	 * @param wait
	 */
	public void setConfiguration(int maxtotal, int wait) {
		bdp.setMaxTotal(maxtotal);
		bdp.setInitialSize(maxtotal);
		bdp.setMaxWaitMillis(wait);
	}
}// class
