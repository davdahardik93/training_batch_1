package com.peacockTech;

import com.peacocktech.OperationMethod;

import java.util.logging.Logger;

import com.peacocktech.DBCPool;

import junit.framework.TestCase;

public class DBCPtest extends TestCase {
	Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private OperationMethod connectionPool = new OperationMethod();
	// in it thread size with 1000 thread
	int numberofThread = 100;
	// init pool size with 10
	int poolSize = 5;
	// init wait time with 1s
	int waitTime = 100;

	public double countElaspedTime(float startTime) {
		float now = System.currentTimeMillis();
		return (now - startTime) / 1000;
	}

	public void testInsertDataThread() {
		DBCPool.getInstance().setConfiguration(poolSize, waitTime);
		float startTime = System.currentTimeMillis();
		connectionPool.insertThread(numberofThread);
		float time=(float) countElaspedTime(startTime);
		logger.info("elapsed time "+time);	
	}
}
