package com.peacock;

import java.beans.PropertyVetoException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Logger;

import org.apache.commons.dbcp2.BasicDataSource;

/**
 * 
 * @author Peacock
 *
 */
public class DBConnection {

	static final Logger log = Logger.getLogger(com.peacock.DBConnection.class.getName());
	Connection connection = null;
	private static DBConnection dbConection = null;
	private BasicDataSource ds;

	/**
	 * 
	 * @return
	 */
	private DBConnection() throws IOException, SQLException, PropertyVetoException {
		
		Properties prop = new Properties();		
	
		InputStream fis = null;		
		fis = new FileInputStream("src/main/resources/datasource.properties");
		
		prop.load(fis);
		
		ds = new BasicDataSource();
		ds.setDriverClassName(prop.getProperty("DRIVERCLASS"));
		ds.setUrl(prop.getProperty("DBURL"));
		ds.setUsername(prop.getProperty("DBUSERNAME"));
		ds.setPassword(prop.getProperty("DBPASSWORD"));
		
		ds.setMaxTotal(20);
		ds.setMaxWaitMillis(2000);
		ds.setInitialSize(20);	

	}

	public static DBConnection getInstnce() throws IOException{
		if (dbConection == null)
			try {
				dbConection = new DBConnection();
			} 
			
			catch (Exception e) {
				log.info("Exception occured"+e);
			}
		return dbConection;
	}

	public Connection getDBConnection() throws SQLException {
		return ds.getConnection();
	}
}
