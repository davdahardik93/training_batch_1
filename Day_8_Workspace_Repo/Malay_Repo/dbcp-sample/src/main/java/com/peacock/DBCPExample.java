package com.peacock;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

/**
 * 
 * @author Peacock
 *
 */
public class DBCPExample {

	final Logger log = Logger.getLogger(com.peacock.DBCPExample.class.getName());
	private static String sqlSelect = "SELECT * from empdetails";
	private static String sqlCount = "SELECT count(*) from empdetails";
	private PreparedStatement pstmt;
	private Statement stmt;

	/**
	 * 
	 * @param sqlInsert
	 * @return
	 * @throws SQLException
	 */
	public int insert(String sqlInsert) throws SQLException {

		Connection connection = null;
		int rowsAffected = 0;

		try {

			connection = DBConnection.getInstnce().getDBConnection();
		}

		catch (IOException e) {
			log.info("Exception Occured" + e);
		}

		if (connection != null) {
			stmt = connection.createStatement();
		}
		stmt.executeUpdate("INSERT INTO empdetails(EmpName,EmpDesignation,EmpSalary)VALUES" + sqlInsert);

		stmt.close();
		if (connection != null) {
			connection.close();
		}

		return rowsAffected;
	}

	/**
	 *
	 * @return count of number of rows returned
	 * @throws SQLException
	 */
	public int select() throws SQLException {
		Connection connection = null;
		int count = 0;

		try {
			connection = DBConnection.getInstnce().getDBConnection();
		} catch (IOException e) {
			log.info("Exception occured" + e);
		}

		if (connection != null) {
			pstmt = connection.prepareStatement(sqlSelect);
		}
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			rs.getInt(1);
			rs.getString(2);
			rs.getString(3);
			rs.getString(4);
			count++;
		}
		pstmt.close();
		if (connection != null) {
			connection.close();
		}
		return count;
	}

	public int getRows() throws SQLException {
		Connection connection = null;
		int count;

		try {
			connection = DBConnection.getInstnce().getDBConnection();
		} catch (IOException e) {
			log.info("Exception occured" + e);
		}

		if (connection != null) {
			pstmt = connection.prepareStatement(sqlCount);
		}
		ResultSet rs = pstmt.executeQuery();
		rs.first();
		count = rs.getInt("COUNT(*)");
		log.info("Number of entries " + count);
		pstmt.close();

		if (connection != null) {
			connection.close();
		}
		return count;
	}

}
