package com.peacock;

import java.sql.SQLException;
import org.junit.Test;
import junit.framework.TestCase;

public class ThreadTest extends TestCase {
	DBCPExample dbcp = new DBCPExample();
	long seconds = 0;
	DBThread th = new DBThread();

	@Test
	public void testInsertData() throws SQLException {
		long startTime = System.currentTimeMillis();

		dbcp.insert("('Malay','SD','12,000')");

		for (int i = 0; i < 1000; i++) {
			System.out.println("Entering test run");
			Thread t1 = new Thread(th, "Thread No. " + i);
			t1.start();
			seconds = (System.currentTimeMillis() - startTime) / 1000;
		}
		System.out.println(seconds + " Seconds");
	}

	@Test
	public void testSelect() throws SQLException {
		assertEquals(dbcp.getRows(), dbcp.select());
	}
}
