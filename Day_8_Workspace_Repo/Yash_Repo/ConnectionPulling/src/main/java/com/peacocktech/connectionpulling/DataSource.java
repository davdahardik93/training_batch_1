package com.peacocktech.connectionpulling;

import java.beans.PropertyVetoException;
import java.io.FileInputStream;

import java.io.IOException;
import java.io.InputStream;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.dbcp2.BasicDataSource;

/**
 * connection class
 * 
 * @author peacock
 *
 */
public class DataSource {
	static final Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	Connection connection = null;
	private static DataSource datasource;
	private BasicDataSource ds;

	private DataSource() throws IOException, SQLException, PropertyVetoException {
		Properties prop = new Properties();
		InputStream input = null;
		input = new FileInputStream(
				"/WorksPlace/Day_7_Workplace/ConnectionString/ConnectionPulling/src/main/resources/connection.properties");

		// load a properties file
		prop.load(input);

		// get the property value and print it out
		log.info("Datasource");
        
		ds = new BasicDataSource();
		ds.setDriverClassName(prop.getProperty("DRIVER_CLASSNAME"));
		ds.setUrl(prop.getProperty("URL"));
		ds.setUsername(prop.getProperty("USER_NAME"));
		ds.setPassword(prop.getProperty("PASSWORD"));
		ds.setMaxTotal(20);
		ds.setMaxWaitMillis(2000);
		ds.setInitialSize(10);

		log.info("--------------Object Created!!--------------");

	}

	/**
	 * single ton method
	 * 
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 * @throws PropertyVetoException
	 */
	public static DataSource getInstance() throws IOException {
		if (datasource == null)
			try {
				datasource = new DataSource();
			} catch (SQLException e) {
				log.log(Level.SEVERE, "SQl Exception occur", e);

			} catch (PropertyVetoException e) {

				log.log(Level.SEVERE, "NUll Exception occur", e);
			}
		return datasource;

	}

	public Connection getConnection() throws SQLException {
		return ds.getConnection();
	}

}
