package com.peacocktech.connectionpulling;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ConnectionPool class
 * 
 * @author peacock
 *
 */

public class ConnectionPool {

	final Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	String string = "NULL Exception occur";
	String string1 = "SQL Exception occur";
	String string2 = " Exception occur";
	static int count = 0;

	/**
	 * insert method
	 * 
	 * @param query
	 */
	public void insert(String query) {
		Connection con;

		try {
			con = DataSource.getInstance().getConnection();
			Statement st = con.createStatement();
			st.executeUpdate("insert into Employee_Profile (Emp_Name,Emp_Age,Emp_Address,Emp_Email)values" + query);
			st.close();
			con.close();

			log.info("data inserted");

		} catch (NullPointerException e) {
			log.log(Level.SEVERE, "NUll Exception occur", e);
		} catch (SQLException e) {
			log.log(Level.SEVERE, "SQl Exception occur", e);
		} catch (IOException e) {
			log.log(Level.SEVERE, "IO Exception occur", e);
		}
	}

}
