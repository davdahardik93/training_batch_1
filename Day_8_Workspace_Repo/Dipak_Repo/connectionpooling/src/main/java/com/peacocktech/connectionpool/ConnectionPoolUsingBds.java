package com.peacocktech.connectionpool;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.dbcp2.BasicDataSource;

/**
 * data source class
 *
 * @author Dipak Savaliya
 * @author Gautam Vanani
 * @author Shweta Mehta
 * @author Vipul Dholariya
 */
public class ConnectionPoolUsingBds {

	private static ConnectionPoolUsingBds datasource;
	private BasicDataSource bds;
	static int count = 0;
	static final Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private ConnectionPoolUsingBds() throws IOException, SQLException {
		// configuration of database connection
		bds = new BasicDataSource();
		// counter for get number of objectF
		count++;
		log.info("done obj " + count);
		GetProperty ge = new GetProperty();
		bds.setUsername(ge.getUserName());// ge.username
		bds.setPassword(ge.getPassword());//
		bds.setUrl(ge.getUri());// "jdbc:mysql://localhost:3306/Name"
		bds.setDriverClassName(ge.getDriver());// "com.mysql.jdbc.Driver"
		// below settings for dbcp can work with defaults
		bds.setInitialSize(ge.getPoolSize());
		bds.setMaxTotal(ge.getPoolSize());
		bds.setMaxIdle(10);
		bds.setMaxWaitMillis(ge.getWaitTimeForThread());
		// getConnection();
	}// cons()

	/**
	 * method to get instance of data source
	 * 
	 * @return object of data source
	 * @throws IOException
	 */
	public static ConnectionPoolUsingBds getInstance() throws IOException {
		if (datasource == null)
			try {
				datasource = new ConnectionPoolUsingBds();
			} catch (SQLException e) {
				log.log(Level.SEVERE, "SQL exception", e);
			}
		return datasource;

	}// get instance

	/**
	 * get instance of connection
	 * 
	 * @return connection object for pool
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException {
		return bds.getConnection();
	}

	/**
	 * set pool size and wait mills for pool
	 * 
	 * @param pool
	 * @param wait
	 */
	public void setConfiguration(int pool, int wait) {
		bds.setInitialSize(pool);
		bds.setMaxTotal(pool);
		bds.setMaxWaitMillis(wait);
	}// set config
}// class
