package com.peacock.crudoperation;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.peacocktech.connectionpool.ConnectionPoolUsingBds;

/**
 * crud method using multi-thread
 * 
 * @author Dipak Savaliya
 * @author Gautam Vanani
 * @author Shweta Mehta
 * @author Vipul Dholariya
 */
public class CrudMethods {

	final Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	static final String IO_EXECPTION = "IO Exception occur";
	static final String SQL_EXCEPTION = "SQl Exception occur";
	static final String NULL_EXCEPTION = "NUll Exception occur";

	/**
	 * insert data name ,dept_id and salary
	 * 
	 * @param name
	 * @param id
	 * @param salary
	 * @return
	 */
	public int insertData(String name, int id, int salary) {
		Connection con;
		String sql = "INSERT INTO Employee(empName,departId,salary) VALUES('" + name + "'," + id + "," + salary + ")";
		try {
			con = ConnectionPoolUsingBds.getInstance().getConnection();
			Statement st = con.createStatement();
			st.executeUpdate(sql);
			st.close();
			con.close();
			return 1;
		} catch (NullPointerException e) {
			log.log(Level.SEVERE, NULL_EXCEPTION, e);
		} catch (SQLException e) {
			log.log(Level.SEVERE, SQL_EXCEPTION, e);
		} catch (IOException e) {
			log.log(Level.SEVERE, IO_EXECPTION, e);
		}
		return 0;
	}

	/**
	 * retrive data...
	 * 
	 * @param query
	 * @param con
	 * @return
	 */
	public Boolean getData(String query) {
		Connection con;
		try {
			con = ConnectionPoolUsingBds.getInstance().getConnection();
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				displayMsg(rs.getString("empName") + " " + rs.getString("departId") + " " + rs.getString("salary"));
			}
			rs.close();
			st.close();
			return true;
		} catch (NullPointerException e) {
			log.log(Level.SEVERE, NULL_EXCEPTION, e);
		} catch (SQLException e) {
			log.log(Level.SEVERE, SQL_EXCEPTION, e);
		} catch (IOException e) {
			log.log(Level.SEVERE, IO_EXECPTION, e);
		}
		return false;
	}

	/**
	 * Thread method for insert
	 * 
	 * @param count
	 */
	public void threadInsert(int count) {
		for (int i = 0; i < count; i++) {
			final int threadnum = i;
			MyThread r = new MyThread(threadnum);
			Thread thread = new Thread(r);
			thread.start();
		}
	}

	private void displayMsg(String msg) {
		log.info(msg);
	}// insert

	/**
	 * This class implements run method for insert data
	 * 
	 * @author Dipak Savaliya
	 * @author Gautam Vanani
	 * @author Shweta Mehta
	 * @author Vipul Dholariya
	 */
	class MyThread implements Runnable {
		private int threadNum;

		public MyThread(int threadnum) {
			this.threadNum = threadnum;
		}

		@Override
		public void run() {
			insertData("thread-" + threadNum, threadNum, 100000);
		}
	}// thread class
}// class