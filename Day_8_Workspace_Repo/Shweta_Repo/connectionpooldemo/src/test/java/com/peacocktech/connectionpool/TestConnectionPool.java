package com.peacocktech.connectionpool;

import java.io.IOException;

import org.junit.Test;

import com.peacock.crudoperation.CrudMethods;

/**
 * connection pool test class using override
 * 
 * @author Dipak Savaliya
 * @author Gautam Vanani
 * @author Shweta Mehta
 * @author Vipul Dholariya
 *
 */
public class TestConnectionPool {

	private CrudMethods connectionPool = new CrudMethods();
	// in it thread size with 1000 thread
	int threadSize = 50000;
	// init pool size with 10
	int poolSize = 60;
	// init wait time with 1s
	int waitTime = 10000;

	/**
	 * calculate milis to sec
	 * 
	 * @param startTime
	 * @return
	 */
	public double countElaspedTime(long startTime) {
		long now = System.currentTimeMillis();
		return (now - startTime) / 1000.0;
	}// elapse time

	/**
	 * 1st thread size, pool size and wait time to test for different case
	 */
	@Test
	public void testInsertDataThread() {
		try {
			ConnectionPoolUsingBds.getInstance().setConfiguration(poolSize, waitTime);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		connectionPool.insertData("root", 1, 10000);
		long startTime = System.currentTimeMillis();
		connectionPool.threadInsert(threadSize);
		System.out.println(countElaspedTime(startTime));
	}// test insert using thread
}// class test
