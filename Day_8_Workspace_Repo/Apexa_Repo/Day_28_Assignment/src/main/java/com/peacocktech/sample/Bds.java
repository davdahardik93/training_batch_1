package com.peacocktech.sample;

import java.util.Properties;
import java.util.logging.Logger;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbcp2.BasicDataSource;

/**
 * 
 * @author Apexa
 * @author Foram
 * @author Vacha
 *
 */
public class Bds {
	private final Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private static Bds mySqlDataSource = null;
	private BasicDataSource basicDataSource = null;

	private Bds() throws IOException {

		File file = new File("src/main/resource/data.properties");
		FileInputStream fileInput = new FileInputStream(file);
		Properties properties = new Properties();
		properties.load(fileInput);
		fileInput.close();

		basicDataSource = new BasicDataSource();
		basicDataSource.setDriverClassName(properties.getProperty("driver"));
		basicDataSource.setUrl(properties.getProperty("url"));
		basicDataSource.setUsername(properties.getProperty("username"));
		basicDataSource.setPassword(properties.getProperty("password"));
		basicDataSource.setInitialSize(Integer.parseInt(properties.getProperty("intialsize")));
		basicDataSource.setMaxIdle(Integer.parseInt(properties.getProperty("maxidle")));
		basicDataSource.setMaxTotal(Integer.parseInt(properties.getProperty("maxtotal")));
		basicDataSource.setMaxWaitMillis(Integer.parseInt(properties.getProperty("maxwait")));
	}// private cons()

	/**
	 * method for set up BDS and return the object of BDS
	 * 
	 * @return
	 * @throws IOException 
	 */
	public static Bds setUpDataSource() throws IOException {
		if (mySqlDataSource == null) {
			mySqlDataSource = new Bds();
		}
		return mySqlDataSource;
	}// getter method for MySqlDataSource instance

	/**
	 * for getting connection using bds
	 * 
	 * @return
	 */
	public Connection getConnection() {
		try {
			return basicDataSource.getConnection();
		} catch (SQLException e) {
			log.info("SQLException " + e);
		}
		return null;
	}// for get connection

}// data source class
