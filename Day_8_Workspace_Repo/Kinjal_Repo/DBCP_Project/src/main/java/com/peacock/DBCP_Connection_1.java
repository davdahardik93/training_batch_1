package com.peacock;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Logger;

import org.apache.commons.dbcp.BasicDataSource;
/**
 * 
 * @author Disha Mehta
 * @author Kinjal Mori
 *
 */
public class DBCP_Connection_1 implements Runnable {
	static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private static BasicDataSource connection;

	
	public static BasicDataSource Dbcp_Connectiond() throws IOException, ClassNotFoundException, SQLException {
	
		if (connection==null){
		logger.info("Object Created");
		Properties prop = new Properties();
		FileInputStream fin = new FileInputStream("/Day_8_Workspace/DBCP_Project/DBCPsources/datasource.properties");
		prop.load(fin);

	
		connection = new BasicDataSource();
	
	
		connection.setUrl(prop.getProperty("Url"));
		connection.setDriverClassName(prop.getProperty("DriverClassName"));
		logger.info("Url done");
		connection.setUsername(prop.getProperty("user"));
		connection.setPassword(prop.getProperty("password"));
		logger.info("connection done");
	
		
		connection.setMaxActive(50);
		connection.setMaxWait(10000);
	
		}
		
		return connection;
	}
/**
 * Run Method
 */
	public void run()   {
		

		Insert_Element insertElement=new Insert_Element();
		logger.info("Entered Run");
		try {
			insertElement.insert_element("Disha");
		} catch (ClassNotFoundException e) {
			logger.info("Exception occured - ClassNotFoundException");
			
		} catch (IOException e) {
			logger.info("Exception occured - IOException");
			
		} catch (SQLException e) {
			logger.info("Exception occured");
			
		}
		
		
		
		
	}
}
