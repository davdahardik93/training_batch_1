package com.peacocktech.dbtest;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Logger;

import org.apache.commons.dbcp.BasicDataSource;

/**
 * Hello world! @
 */
public class MyDataSource {
	InputStream is;
	static BasicDataSource datasource;
	private static MyDataSource ds = null;
	Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	/**
	 * MyDataSource is a Constructor
	 * 
	 * @throws IOException
	 */

	public MyDataSource() throws IOException {
		datasource = new BasicDataSource();
		Properties prop = new Properties();
		is = MyDataSource.class.getClassLoader().getResourceAsStream("datasource.properties");
		prop.load(is);
		datasource.setDriverClassName(prop.getProperty("JDBC_DRIVER"));
		datasource.setUsername(prop.getProperty("USERNAME"));
		datasource.setPassword(prop.getProperty("PASSWORD"));
		datasource.setUrl(prop.getProperty("URL"));
		datasource.setMaxActive(100);
		datasource.setInitialSize(Integer.parseInt(prop.getProperty("initialsize")));
		datasource.setMaxIdle(Integer.parseInt(prop.getProperty("maxidle")));
		datasource.setMaxWait(Integer.parseInt(prop.getProperty("maxwait")));
		
	}

	/**
	 * insertdata is used for insert data in table
	 * 
	 * @param int
	 *            i is used for value to be insert in table
	 * 
	 * @throws IOException
	 */
	public void insertdata(int i) {
		try {

			PreparedStatement pst;
			Connection con = datasource.getConnection();
			String query = " insert into details (empid) values (?)";
			// create the mysql insert preparedstatement
			pst = con.prepareStatement(query);
			pst.setInt(1, i);
			pst.executeUpdate();
			log.info("Value is :" + i);
			pst.close();
			con.close();
		} catch (Exception ex) {
			log.info("Exception is " + ex);

		}

	}

	/**
	 * @return
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException {
		return ds.getConnection();
	}

	/**
	 * main method
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		MyThread t = new MyThread();
		Thread tnew;
		for (int c = 0; c < 10; c++) {
			tnew = new Thread(t);
			tnew.setName("Thread" + c);
			tnew.start();
		}

	}
}
