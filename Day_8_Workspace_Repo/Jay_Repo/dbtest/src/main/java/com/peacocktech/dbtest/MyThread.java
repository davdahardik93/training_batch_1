package com.peacocktech.dbtest;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * MyThread is a public class for thread
 * 
 * @author peacock
 *
 */

public class MyThread implements Runnable {
	// constructor MyThread
	/**
	 * MyThread is constructor
	 */

	Thread thread;
	MyDataSource ds;
	Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	@Override
	public void run() {

		thread = new Thread();
		try {
			ds = new MyDataSource();
		} catch (IOException e) {
			log.info("Exception in thread class is :" + e);
		}
		ds.insertdata(1);

	}
}
