package com.peacocktech.sample;

import java.io.IOException;
import java.sql.Connection;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

/**
 * @author Apexa
 * @author Foram
 * @author Vacha
 */
public class CrudOperation {
	private final  Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	/**
	 * method for static insert operation
	 * @throws IOException 
	 * 
	 */
	public void insertData() throws IOException {
		Connection conn = Bds.setUpDataSource().getConnection();
		try {
			Statement stmt = conn.createStatement();
			String sql = "insert into Employee values(12, 'vacha', 15000, 'developer')";
			stmt.executeUpdate(sql);
			stmt.close();
			conn.close();
		} catch (SQLException sqlException) {
			log.info("SQL Exception "+sqlException);
		}
	}// insert method

	/**
	 * method that run thread with given count
	 * 
	 * @param count
	 *            number of thread
	 */
	public void insertUsingThread(int count) {
		for (int i = 0; i < count; i++) {
			Thread thread = new Thread(new ThreadCreation());
			thread.start();
		} // for
	}// method for thread insert

	/**
	 * 
	 * @author Vacha
	 * @author Gautam Vanani
	 * 
	 *
	 */
	public class ThreadCreation implements Runnable {
		/**
		 * for insert
		 */
		public void run() {
			try {
				insertData();
			} catch (IOException e) {
				log.info("IO Exception "+e);
			}
		}
	}// inner class for insert thread
}// db op class
