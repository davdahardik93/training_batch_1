package com.peacocktech.sample;

import java.io.IOException;


import org.junit.Test;

/**
 * 
 * @author Apexa
 * @author Foram
 * @author Vacha
 *
 */
public class TestDBOperation {
	/**
	 * test case for multi thread for insert operation 1500000.....
	 * 
	 * @throws IOException
	 */
	@Test
	public void test() throws IOException {
		
		CrudOperation dbOperation = new CrudOperation();
		dbOperation.insertData();
		dbOperation.insertUsingThread(2500000);
		
	}// test
}// class
