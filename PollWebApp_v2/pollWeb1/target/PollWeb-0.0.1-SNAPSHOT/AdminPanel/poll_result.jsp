<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.peacocktech.connectionpool.ConnectionPoolUsingBds"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.sql.ResultSet"%>
<%@ page import="java.sql.Statement"%>
<%@ page import="java.sql.SQLException"%>
<%@ page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Results</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">

<!--Icons-->
<script src="js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span>Poll</span>Admin</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> User <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profile</a></li>
							<li><a href="#"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Settings</a></li>
							<li><a href="#"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
							
		</div><!-- /.container-fluid -->
	</nav>
		
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			
			<li class="active"><a href="charts.html"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Results</a></li>
			<li role="presentation" class="divider"></li>
			<li><a href="login.html"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Login Page</a></li>
		</ul>
		
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Results</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Results</h1>
				<!-----------------------start------------------------------>
				<%
		try {
			Statement stmt;
			ResultSet rs;
			Connection con = ConnectionPoolUsingBds.getInstance().getConnection();
			stmt = con.createStatement();
			ArrayList<String> answer = new ArrayList<String>();
			
			//total participants

			String str = "SELECT count(DISTINCT UserRefId) as count from db_poll.ResultInfo";
			rs = stmt.executeQuery(str);
			rs.next();
			out.println("<div> Total participants : " + rs.getString("count"));

			//latest question

			ResultSet rs1 = stmt
					.executeQuery("select QueDetails from db_poll.QuestionInfo ORDER BY QueId DESC LIMIT 1");
			rs1.next();
			out.println("<div> Question : " + rs1.getString("QueDetails"));

			//latest que_id

			 rs1 = stmt
					.executeQuery("select QueId as qcount from db_poll.QuestionInfo ORDER BY QueId DESC LIMIT 1");
			rs1.next();
			int que_id = rs1.getInt("qcount");

			//total option 
			 str = "select count(DISTINCT AnsReferId) as total_ans from db_poll.ResultInfo where QueReferId="
					+ que_id;
			rs = stmt.executeQuery(str);
			rs.next();
			int total_ans = rs.getInt("total_ans");

			// which answer got how many votes

			str = "select AnsDetails from db_poll.AnswerInfo where QueRefId=" + Integer.toString(que_id);
			System.out.println("Query: " + str);
			rs = stmt.executeQuery(str);
			while (rs.next()) {
				answer.add(rs.getString("AnsDetails"));
			}

			for (int i = 1; i <= total_ans; i++) {
				str = "select count(*) as ans" + i + " from db_poll.ResultInfo where QueReferId=" + que_id
						+ " AND AnsReferId=" + i;
				rs = stmt.executeQuery(str);
				while (rs.next()) {

					out.println("<div>" + answer.get(i - 1) + " :" + rs.getInt("ans" + i) + "</div>");
				}

			}
			rs.close();
			con.close();

		} catch (SQLException sq) {
			System.out.println("exception " + sq);
		}
	%>
				
				<!-----------------------end------------------------------>
				
			</div>
		</div><!--/.row-->	
	</div>
	  

	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script>
		!function ($) {
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
		        $(this).find('em:first').toggleClass("glyphicon-minus");      
		    }); 
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	

</body>
</html>