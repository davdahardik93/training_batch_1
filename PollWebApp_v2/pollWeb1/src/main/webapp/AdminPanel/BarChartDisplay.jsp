<%@page import="com.peacocktech.pollweb.pollresult.PieChartClass"%>
<%@page import="com.peacocktech.pollweb.pollresult.BarChart"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
PieChartClass pi = new PieChartClass();
%>
<script type="text/javascript">

function refreshPage() 
{
        document.forms.formId.submit();
}

</script>
<title>JavaScript Refresh Example</title>
</head>

<body>

<%
               response.setIntHeader("Refresh", 10);
        %>
        <form id="formId">
           <input type="button" onclick="refreshPage()" value="Refresh Page" />
           <br /> <img src="../AdminPanel/chart/BarGraph.png" />
        </form>

</body>
</html>
