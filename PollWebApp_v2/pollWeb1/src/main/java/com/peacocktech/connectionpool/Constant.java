package com.peacocktech.connectionpool;

/**
 * constants for db and poll app
 * 
 * @author Dipak Salavaliya
 * @author Gautam Vanani
 * @author Shweta Mehta
 * @author Vacha Dakwala
 * @author Vipul Dholariya
 *
 */
public class Constant {
	public static String UserIdForSession = null;
	public static String IS_SESSTION = "isSesstion";

	public static final String COUNT = "COUNT";

	///////////// table user detail
	public static final String TABLE_USER_INFO = "user_info";

	public static final String USER_ID = "user_id";
	public static final String USER_NAME = "user_name";
	public static final String USER_EMAIL = "user_email";
	public static final String USER_PWD = "user_pwd";

	public static final String USER_DESIGNATION = "user_designation";
	public static final String USER_WEIGHTAGE = "user_weightage";

	public static final String USER_POLL_STATUS = "poll_status";
	public static final String USER_IS_ADMIN = "is_admin";

	///////////// table Ans Info
	public static final String TABLE_ANS_INFO = "answer_info";

	public static final String ANS_ID = "ans_id";
	public static final String ANS_DETAIL = "ans_detail";
	public static final String ANS_WEIGHTAGE = "ans_weightage";
	public static final String ANS_TYPE = "ans_type";

	public static final String ANS_QUE_REF_ID = "ans_que_ref_id";
	public static final String ANS_POLL_REF_ID = "ans_poll_ref_id";

	/////////// table Que Info
	public static final String TABLE_QUESTION_INFO = "que_info";

	public static final String QUESTION_ID = "que_id";
	public static final String QUESTION_DETAIL = "que_detail";
	public static final String QUESTION_WEIGHTAGE = "que_weightage";
	public static final String QUESTION_TYPE = "que_type";
	public static final String QUESTION_CATAGORY = "que_catagory";

	public static final String QUESTION_POLL_REF_ID = "que_poll_ref_id";

	/////////// table Poll Info
	public static final String TABLE_POLL_INFO = "poll_info";
	public static final String POLL_ID = "poll_id";
	public static final String POLL_DETAIL = "poll_detail";
	public static final String POLL_RECIPENT_DESIGNATION = "recipent_designation";
	public static final String POLL_END_DATE_TIME = "end_date_time";
	public static final String POLL_IS_ANO = "is_ano";

	/////////// table res Info
	public static final String TABLE_RESULT_INFO = "result_info";
	public static final String RESULT_ID = "result_id";
	public static final String RESULT_QUE_REFER_ID = "result_que_ref_id";
	public static final String RESULT_ANS_REFER_ID = "result_ans_ref_id";
	public static final String RESULT_USER_REFER_ID = "result_user_ref_id";
	public static final String RESULT_POLL_REFER_ID = "result_poll_ref_id";
	public static final String RESULT_RESPONSE_TIME = "response_time";
	//new added by result
	

	/////////////////////////////////////////////////// btns
	public static final String BTN_SUBMIT = "Submit";
	public static final String BTN_LOGIN = "Login";
	public static final String BTN_CREATE_QUE = "Create Question";
	public static final String BTN_RESAULT = "Results";
	public static final String BTN_POLL = "Poll";

	/////////////////////////////////////////////////// error msg
	public static final String MSG_ANS_SUBMITED = "Answer submitted!";
	public static final String MSG_USER_NOT_FOUND = "User not found!";
	public static final String MSG_INVALID_USER = "Login creadential are wrong!";
	public static final String MSG_INVALID_ADMIN = "Admin not found!";
	public static final String MSG_POLL_CREATE = "Poll created!";
	public static final String MSG_POLL_DELETE = "Poll deleted!";
	public static final String MSG_ALREADY_POLLED = "Already polled!";
	public static final String MSG_POLL_NOT_EXIST = "Poll doesn't exist!";

	////////////////////////////////////////////// menu name
	public static final String MENU_MANAGE_USER = "Maneger User";
	public static final String MENU_POLL = "Poll";
	public static final String MENU_RESULT = "Result";
	public static final String MENU_LOGOUT = "Logout";
	public static final String MENU_ITEM_ADD_USER = "Add User";
	public static final String MENU_ITEM_REMOVE_USER = "Remove User";
	public static final String MENU_ITEM_CREATE_POLL = "Create Poll";
	public static final String MENU_ITEM_DELETE_POLL = "Delete Poll";
	public static final String MENU_ITEM_DO_POLL = "Do Poll";
	public static final String MENU_ITEM_STATISTICS_RESULT = "Statistics Result";
	public static final String MENU_ITEM_CHART_RESULT = "Chart Result";
}// class
