package com.peacocktech.pollweb.pollresult;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;

import com.peacocktech.connectionpool.ConnectionPoolUsingBds;

/**
 * Servlet implementation class PieChartServlet
 */
public class PieChartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	Statement stmt;
	ResultSet rs, rs1;
	CallableStatement cstmt;
	Connection dbConn = null;
	int poll_id;
	public ArrayList<Integer> queID = new ArrayList<Integer>();
	public ArrayList<String> queDetails = new ArrayList<String>();
	HashMap<String, Integer> pieChartData = new HashMap<String, Integer>();
	ResultSet questionResultSet;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PieChartServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at:
		// ").append(request.getContextPath());
		System.out.println("Question From JSP = " + request.getParameter("selectedQue"));
		// int jspQueId =
		// Integer.parseInt(request.getParameter("selectedQue").trim());
		PrintWriter out = response.getWriter();
		String jspQueId = request.getParameter("selectedQue");
		System.out.println("selected Que = " + jspQueId);

		try {
			dbConn = ConnectionPoolUsingBds.getInstance().getConnection();
			System.out.println("hello");
			cstmt = dbConn.prepareCall("{call db_poll.pollresult_latest_poll()}");
			System.out.println("str pro done" + cstmt);
			rs = cstmt.executeQuery();

			// Get the latest poll_id
			rs.next();
			poll_id = rs.getInt("poll_id");
			System.out.print("poll _id" + poll_id);

			// //Get the Question Details using the latest poll_id
			// cstmt = dbConn.prepareCall("{call
			// db_poll.pollresult_que_details(?)}");
			// cstmt.setInt(1, poll_id);
			// rs = cstmt.executeQuery();
			//
			// while(rs.next())
			// {
			// System.out.println("id "+rs.getInt("que_id")+"question
			// "+rs.getString("que_detail"));
			// //que.put(rs.getInt("que_id"),rs.getString("que_detail"));
			// queID.add(rs.getInt("que_id"));
			// queDetails.add(rs.getString("que_detail"));
			// this.setQuestionResultSet(rs);
			//// this.queID.add(rs.getInt("que_id"));
			//// this.queDetails.add(rs.getString("que_detail"));
			// System.out.println("hello");
			// }
			// rs.first();

			// Get Latest Question from Latest Poll

			// cstmt = dbConn.prepareCall("{call
			// db_poll.pollresult_latest_question_in_poll(?)}");
			// System.out.println("query"+cstmt);
			// cstmt.setInt(1, poll_id);
			// rs = cstmt.executeQuery();
			// System.out.println("Inside Questions from latest poll");
			// rs.next();
			// int latestQue = rs.getInt("que_id");
			// System.out.println("Latest que no is: "+latestQue+" In poll
			// "+poll_id);
			//
			// Get number of options for latest Question

			cstmt = dbConn.prepareCall("{call db_poll.pollresult_get_ans_for_each_que(?, ?)}");
			int queID = Integer.parseInt(jspQueId);
			// System.out.println("Latest question entering query: "+latestQue);
			System.out.println("Latest poll entering query: " + poll_id);
			System.out.println("Question from jsp" + jspQueId);
			cstmt.setInt(1, queID);
			cstmt.setInt(2, poll_id);
			rs = cstmt.executeQuery();
			rs.next();
			int numOfOptions = rs.getInt("number_of_option");
			System.out.println("Total options in current Question are: " + numOfOptions);

			// Clear HashMap every time before populating it with new values so
			// that it does'nt take old values
			pieChartData.clear();

			// Get Number of users per option
			for (int i = 1; i <= numOfOptions; i++) {
				cstmt = dbConn.prepareCall("{call db_poll.pollresult_no_of_user_per_ans_option(?, ?, ?)}");
				cstmt.setInt(1, queID);
				cstmt.setInt(2, i);
				cstmt.setInt(3, poll_id);
				rs1 = cstmt.executeQuery();

				while (rs1.next()) {
					System.out.println("Option " + i);
					System.out.println(rs1.getInt(1));

					pieChartData.put("Option " + i, rs1.getInt(1));
					System.out.println(" ");
					// put(id, rs1.getInt(1));
				}
				// pieChartData.put("Option"+i, 33);

			}
			System.out.println("In pie chart data");
			DefaultPieDataset dataset = new DefaultPieDataset();

			Iterator it = pieChartData.entrySet().iterator();
			while (it.hasNext()) {

				Map.Entry pair = (Map.Entry) it.next();
				System.out.println(pair.getKey() + " = " + pair.getValue());
				String s1 = (String) pair.getKey();
				int i1 = (Integer) pair.getValue();
				dataset.setValue(s1, i1);
				// it.remove(); // avoids a ConcurrentModificationException
			}
			// dataset.setValue("akfem", 22);
			String title = "Question Number: " + Integer.toString(queID);
			// Creating Pie Chart
			JFreeChart chart = ChartFactory.createPieChart(title, // chart title
					dataset, // data
					true, // include legend
					true, false);
			int width = 640; /* Width of the image */
			int height = 480; /* Height of the image */

			PiePlot plot = (PiePlot) chart.getPlot();
			plot.setBackgroundPaint(Color.WHITE);
			plot.setOutlineVisible(false);

			File pieChart = new File(
					"/Training_Workspace/PollWebApp/PollWeb1/src/main/webapp/AdminPanel/chart/Pie_Chart_  "+poll_id+"_"+queID+".png");
			ChartUtilities.saveChartAsJPEG(pieChart, chart, width, height);
			System.out.println("completed...");
			out.println("<img id=\"image\" src=\"../AdminPanel/chart/Pie_Chart.png\">");

			System.out.println("completed...");
			dbConn.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ResultSet getQuestionResultSet() {
		return questionResultSet;
	}

	public void setQuestionResultSet(ResultSet questionResultSet) {
		this.questionResultSet = questionResultSet;
	}

	public ArrayList<Integer> getQueID() {
		return queID;
	}

	public void setQueID(ArrayList<Integer> queID) {
		this.queID = queID;
	}

	public ArrayList<String> getQueDetails() {
		return queDetails;
	}

	public void setQueDetails(ArrayList<String> queDetails) {
		this.queDetails = queDetails;
	}

	public static void main(String[] args) {
		PieChartClass pieChart = new PieChartClass();

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
