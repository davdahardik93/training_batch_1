package com.peacocktech.pollweb.pollresult;

import java.awt.Color;
import java.awt.Paint;
import java.io.File;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map.Entry;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.labels.CategoryItemLabelGenerator;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.BarRenderer3D;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

import com.peacocktech.connectionpool.ConnectionPoolUsingBds;

public class BarChart {
	 
	Connection dbConn = null;
    int count=0;  
    int key=0;
    int value=0;
    Statement stmt;
	ResultSet rs,rs1;
	CallableStatement cstmt,cstmt1;
	HashMap<Integer,Integer> barChartData = new HashMap<Integer, Integer>();
	DefaultCategoryDataset dataset = new DefaultCategoryDataset();
	
	/**
	 * @author Kinjal, Malay
	 * @throws IOException
	 * @return url of the graph
	 */
	public String generateBarGraph() throws IOException
	{
		try {
				dbConn = ConnectionPoolUsingBds.getInstance().getConnection();
			System.out.println("hello");
			 cstmt = dbConn.prepareCall("{call db_poll.pollresult_que_count_latest_poll()}");
			System.out.println("str pro done"+cstmt);
			rs = cstmt.executeQuery();
			while (rs.next()) {
				System.out.println(rs.getInt("que_id"));
				System.out.println("rs====");
				count++;
			}
			System.out.println("Count"+count);
			rs.beforeFirst();
		
    	   while(rs.next())
    	   {
    	   System.out.println("in for");
    	   int id = rs.getInt(1);
    	   System.out.println("Question "+id);
    	   cstmt1=dbConn.prepareCall("{call db_poll.pollresult_participant_per_que(?)}");
    	   cstmt1.setInt(1, id);
    	  rs1= cstmt1.executeQuery();
    	   while (rs1.next()) {
    	   //System.out.println("Votes "+rs1.getInt(1));
    		   barChartData.put(id, rs1.getInt(1));
    	   }
    	   }
    	   rs.beforeFirst();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		 
		 

		 for(Integer queNo: barChartData.keySet())
 	   {
 		    key = queNo.intValue();
 		    value = barChartData.get(queNo).intValue();
 		    dataset.setValue(value, "",Integer.toString(key));
 	   }
		 	       
		 JFreeChart chart = ChartFactory.createBarChart("Result","Questions", "Attempted By", dataset, PlotOrientation.VERTICAL,false, true, false);
		
		 
		 ChartRenderingInfo  info = new ChartRenderingInfo(new StandardEntityCollection());
	        
	        
	        CategoryPlot plot = chart.getCategoryPlot();
	        BarRenderer renderer = (BarRenderer)plot.getRenderer();

	        
	        plot.setBackgroundPaint(Color.WHITE);
	        plot.setDomainGridlinePaint(Color.WHITE);
	        plot.setRangeGridlinePaint(Color.WHITE);
	        plot.setOutlineVisible(false);
	        

	        
	        //Put caption on X-axis
	        renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
	        renderer.setBaseItemLabelsVisible(true);
	        
	        renderer.getBaseItemLabelGenerator();
	        renderer.setBaseItemLabelsVisible(true);
	        

	        
	        //Formatting X-axis
	        CategoryAxis domainAxis = plot.getDomainAxis();
	        domainAxis.setTickLabelPaint(new Color(160,163,165));
	        domainAxis.setCategoryLabelPositionOffset(4);
	        domainAxis.setUpperMargin(0);
	        domainAxis.setLowerMargin(0);
	        domainAxis.setCategoryMargin(0.2);
	        
	        renderer.setGradientPaintTransformer(null);
	        renderer.setBarPainter(new StandardBarPainter());
	        renderer.setMaximumBarWidth(.35);
	        		 
	        		Paint[] colors ={new Color(0, 172, 178), new Color(239, 70, 55),new Color(85, 177, 69)};
	        		          
	        	
	        //Changing Default Colors 
	        		for (int i = 0; i < 2; i++) {
	        		    renderer.setSeriesPaint(i, colors[i % colors.length]);
	        		}
	        
	        try {
	        	if(chart!=null)
	        	{
	        		  ChartUtilities.saveChartAsPNG(new File("/Training_Workspace/PollWebApp/PollWeb/src/main/webapp/AdminPanel/chart/BarGraph.png"), chart, 700, 500,info);	            
	  	            System.out.println("Chart created successfully");
	        	}
	          
	        } catch (IOException e) {
	            System.err.println("Problem occurred creating chart.");
	}
		return "../AdminPanel/chart/BarGraph.png";
	}
	
	 public static void main(String[] args) {
		 
		 BarChart chart = new BarChart();
		 try {
			chart.generateBarGraph();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
