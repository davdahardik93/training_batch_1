package com.peacock.mycircularqueue;

import java.util.logging.Logger;

/**
 * use method of queue
 * 
 * @author peacock
 *
 */
public class MyQueueMethod {
	private MyQueue frontNode = null;
	private MyQueue rearNode = null;
	private int counter = 0;

	public int getCouneter() {
		return counter;
	}

	public void clearMyQueue() {
		frontNode = null;
		rearNode = null;
	}

	protected int valueToFind;
	protected static final String TAG = MyQueueMethod.class.getName();

	/**
	 * method to add Node of given value
	 * 
	 * @param value
	 */
	public void addNode(int value) {
		MyQueue newNode = new MyQueue(value);
		if (frontNode == null) {
			frontNode = newNode;
			rearNode = newNode;
			printLog("front node added with-" + value);
		} // for 1st node
		else {

			newNode.setNext(frontNode);
			frontNode.setPrevious(newNode);
			rearNode.setNext(newNode);
			newNode.setPrevious(rearNode);
			rearNode = newNode;
			printLog("node added at rear with-" + value);
		}
		counter++;
	}

	/**
	 * delete node from front
	 */
	public void deleteNode() {
		if (frontNode != null) {
			MyQueue temp = frontNode;
			if (temp.getNext() == null) {
				frontNode = rearNode = null;
				printLog("My quque is empty");
				counter = 0;
			} else {
				printLog("deleted -" + frontNode.getValue());
				frontNode = frontNode.getNext();
				frontNode.setPrevious(rearNode);
				counter--;
			}
		}
		printLog("Queue is already empty");
	}

	/**
	 * method to traverse and find given element in given direction
	 * 
	 * @param value
	 * @param isReverse
	 *            true for find in reverse direction
	 * @return
	 */
	public boolean traverseQueue(int value, boolean isReverse) {
		if (frontNode == null) {
			printLog("My quque is empty");
			return false;
		}
		if (isReverse) {// for reverse
			MyQueue temp = rearNode;
			while (temp != null && temp != frontNode) {
				printLog("rev-" + Integer.toString(temp.getValue()));
				if (temp.getValue() == value) {
					return true;
				}
				temp = temp.getPrevious();
			} // while
			if (temp.getValue() == value) {
				return true;
			} // check last node

		} else {
			MyQueue temp = frontNode;
			while (temp != null && temp != rearNode) {
				printLog("rev-" + Integer.toString(temp.getValue()));
				if (temp.getValue() == value) {
					return true;
				}
				temp = temp.getPrevious();
			} // while
			if (temp.getValue() == value) {
				return true;
			} // check last node
		}
		return false;
	}

	/**
	 * 
	 * method will print info log in console with given message
	 * 
	 * @param msg
	 */
	public void printLog(String msg) {
		Logger logger = Logger.getLogger(TAG);
		logger.info(msg);
	}
}
