package com.peacock.mycircularqueue;

/**
 * MyQueue
 * 
 * @author peacock
 *
 */

public class MyQueue {
	private int value;
	private MyQueue nextNode, previousNode;

	/**
	 * default cons for value
	 * 
	 * @param value
	 */
	public MyQueue(int value) {
		this.value = value;
		nextNode = null;
		previousNode = null;

	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public MyQueue getNext() {
		return nextNode;
	}

	public void setNext(MyQueue next) {
		this.nextNode = next;
	}

	public MyQueue getPrevious() {
		return previousNode;
	}

	public void setPrevious(MyQueue previous) {
		this.previousNode = previous;
	}

}
