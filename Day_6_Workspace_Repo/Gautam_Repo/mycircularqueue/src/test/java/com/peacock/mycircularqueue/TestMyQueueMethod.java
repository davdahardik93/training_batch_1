package com.peacock.mycircularqueue;

import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
public class TestMyQueueMethod extends TestCase {
	MyQueueMethod myQueueMethod;

	/**
	 * Create the test case
	 *
	 * @param testName
	 *            name of the test case
	 */
	public TestMyQueueMethod(String testName) {
		super(testName);
		myQueueMethod = new MyQueueMethod();
	}

	/**
	 * test case for add using counter
	 */
	public void testAdd() {
		myQueueMethod.clearMyQueue();
		myQueueMethod.addNode(10);
		assertEquals(1, myQueueMethod.getCouneter());
	}

	/**
	 * test case for add fail using counter
	 */
	public void testFailAdd() {
		myQueueMethod.clearMyQueue();
		myQueueMethod.addNode(10);
		assertNotSame(0, myQueueMethod.getCouneter());
	}

	/**
	 * test case for add using traverse r and delete
	 */
	public void testAddUsingTraverseRDelete() {
		myQueueMethod.clearMyQueue();
		myQueueMethod.addNode(10);
		myQueueMethod.deleteNode();
		assertEquals(false, myQueueMethod.traverseQueue(10, true));
	}

	/**
	 * test case for add fail using traverse r Delete
	 */
	public void testFailAddUsingTraverseRDelete() {
		myQueueMethod.clearMyQueue();
		myQueueMethod.addNode(10);
		myQueueMethod.deleteNode();
		assertNotSame(true, myQueueMethod.traverseQueue(30, true));
	}

	/**
	 * test case for add using traverse front Delete
	 */
	public void testAddUsingTraverseFDelete() {
		myQueueMethod.clearMyQueue();
		myQueueMethod.deleteNode();
		myQueueMethod.addNode(10);
		assertEquals(true, myQueueMethod.traverseQueue(10, true));
	}

	/**
	 * test case for add fail using traverse front Delete
	 */
	public void testFailAddUsingTraverseFDelete() {
		myQueueMethod.clearMyQueue();
		myQueueMethod.addNode(10);
		myQueueMethod.deleteNode();
		assertNotSame(true, myQueueMethod.traverseQueue(30, true));
	}

	/**
	 * test case for add fail using traverse r
	 */
	public void testFailAddUsingTraverseR() {
		myQueueMethod.clearMyQueue();
		myQueueMethod.addNode(10);
		assertEquals(false, myQueueMethod.traverseQueue(30, true));
	}

	/**
	 * test case for add using traverse front
	 */
	public void testAddUsingTraverseF() {
		myQueueMethod.clearMyQueue();
		myQueueMethod.addNode(10);
		assertEquals(true, myQueueMethod.traverseQueue(10, true));
	}

	/**
	 * test case for add fail using traverse front
	 */
	public void testFailAddUsingTraverseF() {
		myQueueMethod.clearMyQueue();
		myQueueMethod.addNode(10);
		assertEquals(false, myQueueMethod.traverseQueue(30, true));
	}

	/**
	 * test case for del
	 */
	public void testDel() {
		myQueueMethod.clearMyQueue();
		myQueueMethod.addNode(10);
		myQueueMethod.addNode(20);
		myQueueMethod.deleteNode();
		assertEquals(1, myQueueMethod.getCouneter());
	}

	/**
	 * test case for del fail
	 */
	public void testFailDel() {
		myQueueMethod.clearMyQueue();
		myQueueMethod.addNode(10);
		myQueueMethod.deleteNode();
		assertNotSame(2, myQueueMethod.getCouneter());
	}

	/**
	 * test case for traverse reverse
	 */
	public void testTraverseR() {
		myQueueMethod.clearMyQueue();
		myQueueMethod.addNode(10);
		myQueueMethod.addNode(20);
		myQueueMethod.addNode(30);
		myQueueMethod.addNode(40);
		myQueueMethod.addNode(50);

		myQueueMethod.printLog("travers-r");
		assertEquals(true, myQueueMethod.traverseQueue(10, true));
	}

	/**
	 * test case for traverse fail reverse
	 */
	public void testFailTraverseR() {
		myQueueMethod.clearMyQueue();
		myQueueMethod.addNode(20);
		myQueueMethod.addNode(30);
		myQueueMethod.addNode(40);
		myQueueMethod.addNode(50);
		myQueueMethod.printLog("travers-r-fail");
	}

	/**
	 * test case for traverse
	 */
	public void testTraverseF() {
		myQueueMethod.clearMyQueue();
		myQueueMethod.addNode(10);
		myQueueMethod.addNode(20);
		myQueueMethod.addNode(30);
		myQueueMethod.addNode(40);
		myQueueMethod.addNode(50);
		myQueueMethod.printLog("travers-f");
		assertEquals(true, myQueueMethod.traverseQueue(50, false));
	}

	/**
	 * test case for traverse fail
	 */
	public void testFailTraverseF() {
		myQueueMethod.addNode(20);
		myQueueMethod.addNode(30);
		myQueueMethod.addNode(40);
		myQueueMethod.addNode(50);
		myQueueMethod.printLog("travers-f-fail");
		assertNotSame(true, myQueueMethod.traverseQueue(10, false));
	}

}
