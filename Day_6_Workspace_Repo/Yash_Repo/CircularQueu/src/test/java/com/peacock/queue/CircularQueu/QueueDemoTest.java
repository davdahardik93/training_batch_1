package com.peacock.queue.CircularQueu;

import com.peacock.circularqueue.Queue;

import junit.framework.TestCase;

public class QueueDemoTest extends TestCase {
	public QueueDemoTest() {
		super();
	}

	Queue sm=new Queue();
	
	public void testAddValue(){
		sm.addvalue(10);
		sm.addvalue(25);
		assertEquals(2,sm.getSize());
	}
	public void testfailadd(){
		sm.addvalue(10);
		sm.addvalue(67);
		sm.addvalue(11);
		assertNotSame(4, sm.getSize());
	}
	public void testdelete(){
		sm.addvalue(10);
		sm.addvalue(67);
		sm.delete();
	    assertEquals(1,sm.getSize());
	    
	}
	public void testfaildelete(){
		sm.addvalue(10);
		sm.addvalue(20);
		sm.addvalue(90);
		sm.delete();
		assertNotSame(4, sm.getSize());
	}
	public void testfsearch(){
		sm.addvalue(20);
		sm.addvalue(45);
		sm.addvalue(35);
		assertNotSame(true, sm.frontsearch(55));
		
	}
	public void testfremovesearch(){
		sm.addvalue(22);
		sm.addvalue(34);
		sm.delete();
		assertEquals(false, sm.frontsearch(22));
	}
	public void testrsearch(){
		sm.addvalue(4);
		sm.addvalue(44);
		sm.addvalue(5);
		assertEquals(true,sm.rearsearch(5));
	}
	public void testrremovesearch(){
		sm.addvalue(34);
		sm.delete();
		assertNotSame(true, sm.rearsearch(45));
	}
	public void testaddremoveitem(){
		sm.addvalue(23);
		sm.addvalue(56);
		sm.addvalue(67);
		sm.delete();
	    sm.delete();
	    assertEquals(false,sm.frontsearch(23));
	}
	public void testfserch(){
		sm.addvalue(32);
		sm.addvalue(67);
		assertEquals(true, sm.frontsearch(67));
	}
	

	
}
