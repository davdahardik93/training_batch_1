package com.peacock.queue.CircularQueu;

import java.util.logging.Logger;
/**
 * structure of queue
 * @author peacock
 *
 */
class Node{
	private int value;
	private Node left;
	private Node right;
	
	
	public int getValue() {
		return value;
	}


	public void setValue(int value) {
		this.value = value;
	}


	public Node getLeft() {
		return left;
	}


	public void setLeft(Node left) {
		this.left = left;
	}


	public Node getRight() {
		return right;
	}


	public void setRight(Node right) {
		this.right = right;
	}


	public Node(int value,Node left,Node right)
	{
		this.value=value;
		this.left=left;
		this.right=right;
	}
	
}
/**
 * class sample where create Add,delete,traverse method
 * @author peacock
 *
 */
 class Sample {
		Node front;
		Node rear;
		
		
		private final static Logger LOGGER = Logger.getLogger(Sample.class.getName());
		private int size = 0;
		
		public int getSize() {
			return size;
		}
		
		/**
		 * define Add method
		 * @param value
		 */
		public void addvalue(int value)
		{
			Node newnode=new Node(value, null, null);
			if (front == null && rear == null) {
				front = newnode;
				rear = newnode;
				size++;
			} else {	
				newnode.setRight(front);
				newnode.setLeft(rear);
				rear.setRight(newnode);
				
				front.setLeft(newnode);
				rear = newnode;
				size++;
			}
			LOGGER.info("value"+ newnode.getValue());
			LOGGER.info("size"+ getSize());
			
		}
		/**
		 * define delete method
		 * @param value
		 */
		 void delete(){
			Node var=front;
			 
					 if(front==null && rear==null )
					 {
						 LOGGER.info("Queue is empty");
					 }
					 else if(front!=null && front.equals(rear)){
						front=null;
						rear=null;
						size--;
					 }
					 else{
						if(var!=null){
							front=var.getRight();
							front.setRight(rear);
							size--;
						}
						 
					 }
		 }
		 /**
		  * define traverse method
		  * @param value
		  * @return
		  */
		 boolean frontsearch(int value) {
			 Node temp = front;

				while (temp != rear && temp!=null) {

					if (temp.getValue() == value) {
						LOGGER.info("value:" + temp.getValue());
						return true;
					}
					temp = temp.getRight();
				}
				if (temp!=null && temp.getValue() == value) {
					LOGGER.info("value1:" + temp.getValue());
					return true;
				}

				return false;
			}
		 /**
		  * define traverse method
		  * @param value
		  * @return
		  */
		 boolean rearsearch(int value){
			 Node temp=rear;
			 while( temp!=front){
				 if(temp.getValue()==value){
					 LOGGER.info("value"+temp.getValue());
					 return true;
				 }
				 temp=temp.getRight();
			 }
			 if(temp!=null && temp.getValue()==value)
			 {
				 LOGGER.info("value1"+temp.getValue());
				 return true;
			 }
			 return false;
		 }
		
}

