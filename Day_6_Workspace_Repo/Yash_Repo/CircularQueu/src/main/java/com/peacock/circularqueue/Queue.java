package com.peacock.circularqueue;

import java.util.logging.Logger;


class Node{
	private int value;
	private Node left;
	private Node right;
	/**
	 * constructor create
	 * @param value
	 * @param left
	 * @param right
	 */
		public Node(int value,Node left,Node right)
		{
			this.value=value;
			this.left=left;
			this.right=right;
		}
		
	
	public int getValue() {
		return value;
	}


	public void setValue(int value) {
		this.value = value;
	}


	public Node getLeft() {
		return left;
	}


	public void setLeft(Node left) {
		this.left = left;
	}


	public Node getRight() {
		return right;
	}


	public void setRight(Node right) {
		this.right = right;
	}


}
/**
 * class queue
 * @author peacock
 *
 */
public class Queue {
	Node front;
	Node rear;
	
	
	 Logger log = Logger.getLogger(Queue.class.getName());
	private int size = 0;
	
	public int getSize() {
		return size;
	}
	
	/**
	 * define Add method
	 * @param value
	 */
	public void addvalue(int value)
	{
		Node newnode=new Node(value, null, null);
		if (front == null && rear == null) {
			front = newnode;
			rear = newnode;
			size++;
		} else {	
			newnode.setRight(front);
			newnode.setLeft(rear);
			rear.setRight(newnode);
			if(front!=null){
			front.setLeft(newnode);
			rear = newnode;
			size++;
		}
		}
		log.info("value"+ newnode.getValue());
		log.info("size"+ getSize());
		
	}
	/**
	 * define delete method
	 * @param value
	 */
	 public void delete(){
		Node var=front;
		 
				 if(front==null && rear==null )
				 {
					 log.info("Queue is empty");
				 }
				 else if(front!=null && front.equals(rear)){
					front=null;
					rear=null;
					size--;
				 }
				 else{
					if(var!=null){
						front=var.getRight();
						front.setRight(rear);
						size--;
					}
					 
				 }
	 }
	 /**
	  * define traverse method
	  * @param value
	  * @return
	  */
	public boolean frontsearch(int value) {
		 Node temp = front;

			while (temp != rear && temp!=null) {

				if (temp.getValue() == value) {
					log.info("value:" + temp.getValue());
					return true;
				}
				temp = temp.getRight();
			}
			if (temp!=null && temp.getValue() == value) {
				log.info("value1:" + temp.getValue());
				return true;
			}

			return false;
		}
	 /**
	  * define traverse method
	  * @param value
	  * @return
	  */
	public boolean rearsearch(int value){
		 Node temp=rear;
		 while( temp!=front){
			 if(temp.getValue()==value){
				 log.info("value"+temp.getValue());
				 return true;
			 }
			 temp=temp.getRight();
		 }
		 if(temp!=null && temp.getValue()==value)
		 {
			 log.info("value1"+temp.getValue());
			 return true;
		 }
		 return false;
	 }
	
}
