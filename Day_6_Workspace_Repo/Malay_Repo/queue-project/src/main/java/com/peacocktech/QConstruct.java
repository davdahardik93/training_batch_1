package com.peacocktech;

import java.util.logging.Logger;

/**
 * 
 * @author Peacock
 *
 */
class QElement {
	int value;
	QElement nextNode;
	QElement prevNode;
	
	/**
	 * 
	 * @param value
	 * @param nextNode
	 * @param prevNode
	 */
	public QElement(int value, QElement nextNode, QElement prevNode)
	{
		this.value = value;
		this.nextNode = nextNode;
		this.prevNode = prevNode;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public QElement getNextNode() {
		return nextNode;
	}

	public void setNextNode(QElement nextNode) {
		this.nextNode = nextNode;
	}

	public QElement getPrevNode() {
		return prevNode;
	}

	public void setPrevNode(QElement prevNode) {
		this.prevNode = prevNode;
	}
	
	
}

public class QConstruct{
	
	private Logger log = Logger.getLogger(com.peacocktech.QConstruct.class.getName());
	QElement front = null;
	QElement rear = null;
	int size = 0;
	
	
	public int getSize() {
		return size;
	}

	/**
	 * 
	 * @param input
	 */
	public void add(int input)
	{
		QElement node = new QElement(input, null, null);
		
		if(rear==null && front==null)
		{
			rear = node;
			front = node;
			size++;
		}
		
		else
		{
			node.setNextNode(front);
			node.setPrevNode(rear);
			
			if(rear!= null)
			{
				rear.setNextNode(node);
			}	
			
			rear = node;
			
			front.setPrevNode(rear);
			
			size++;
		}
		log.info("You inserted: "+ node.getValue());
	}
	
	/**
	 * This will remove elements from queue
	 */
	public void remove()
	{
		QElement temp = front;
		
		if(front == null && rear == null)
		{
			log.info("Can not delete from an empty queue");
		}
		
		else if((front!=null)&&(front.equals(rear)))
		{
			front = null;
			rear = null;
			size--;
		}
		else
		{
			if(temp!=null)
			{	
				front =temp.getNextNode();
				front.setPrevNode(rear);
			}
			size--;
		}
	}
	
	/**
	 * For searching from front end of queue
	 * @param value
	 * @return
	 */
	public boolean frontTraverse(int value)
	{
		QElement temp = front;
		while(temp!=null && temp!=rear)
		{
			if(temp.getValue()==value)
				{
					log.info("Found the required element"+ temp.getValue());
					return true;
				}
			temp = temp.getNextNode();
		}
		
		if(temp.getValue()==value)
		{
			log.info("Found the required element at front: "+temp.getValue());
			return true;
		}
		return false;
	}
	
	/**
	 * For searching from rear end of queue
	 * @param value
	 * @return
	 */
	boolean rearTraverse(int value)
	{
		QElement temp = rear;
		while(temp!=null && temp!=front)
		{
			if(temp.getValue()==value)
				{
					log.info("Found the required element"+ temp.getValue());
					return true;
				}
			temp = temp.getPrevNode();
		}
		
		if(temp.getValue()==value)
		{
			log.info("Found the required element at rear end: ");
			return true;
		}
		return false;
	}
}