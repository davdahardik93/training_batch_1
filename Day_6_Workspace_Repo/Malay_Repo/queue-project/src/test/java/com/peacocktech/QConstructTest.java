package com.peacocktech;

import junit.framework.TestCase;

public class QConstructTest extends TestCase {
	QConstruct queue = new QConstruct();
	
	  public void testadd() {
		  queue.add(20);
		  queue.add(30);
		  queue.add(50);
		  assertEquals(queue.frontTraverse(20), true);
		  assertEquals(queue.frontTraverse(30), true);
		  assertEquals(queue.frontTraverse(50), true);
		  assertEquals(queue.size, 3);

	  }
	   public void testaddFail() {
			  queue.add(20);
			  queue.add(30);
			  queue.add(50);
			  assertNotSame(queue.frontTraverse(20), false);
			  assertNotSame(queue.rearTraverse(30), false);
			  assertNotSame(queue.size, 2);
		  }
	   public void testDelete() {
		   queue.add(20);
		   queue.add(30);
		   queue.add(50);
		   queue.remove();
		   assertEquals(queue.frontTraverse(20), false); 
		   assertEquals(queue.size, 2);
	   }
	   public void testDeleteFail() {
		   queue.add(20);
		   queue.add(30);
		   queue.add(50);
		   queue.remove();
		   assertNotSame(queue.rearTraverse(20), true);
		   assertNotSame(queue.size, 3);
	   }
	   public void testFront() {
		   queue.add(15);
		   queue.add(18);
		   assertEquals(queue.frontTraverse(18), true);
	   }
	   public void testFrontFail() {
		   queue.add(15);
		   queue.add(23);
		   assertNotSame(queue.frontTraverse(17), true);
	   }
	   public void testRear() {
		   queue.add(15);
		   queue.add(18);
		   queue.add(27);
		   assertEquals(queue.rearTraverse(18), true);
		   assertEquals(queue.rearTraverse(27), true);
	   }
	   public void testRearFail() {
		   queue.add(15);
		   queue.add(18);
		   assertNotSame(queue.rearTraverse(27), true);
	   }

}

