package com.peacocktech.cirularqeue;
import java.util.logging.Logger;
import com.peacocktech.cirularqeue.Node;

/**
 * 
 * @author peacock
 *
 */
public class QueueList {
 
	Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	Node root;
	Node last;
	int size =0 ;
	/**
	 * 
	 * @param value
	 */
	public void addnode(int value)
	{
		
		

		if(root == null)
		{
			root = new Node(value);
			last = root;
			log.info("root added: " +value);
		}
		else
		{
			Node node = new Node(value);
			node.setLeft(last);
			node.setRight(root);
			last.setRight(node);
			root.setLeft(node);
			last = node;
			log.info("value added: " +value);
		}
		size++;
	}
	
	/**
	 * @return
	 * 
	 */
	public int remove()
	{
		if(root != null)
		{
			
			int value = root.getValue();
			log.info("value removed: " +root.getValue());
			size--;
			if(last == root)
			{
				last = null;
				root = null;
			}
			else
			{
				Node temp = root.getRight();
				if(last == temp)
				{
					last.setLeft(null);
					last.setRight(null);
					root = last;
				}
				else
				{
					last.setRight(temp);
					temp.setLeft(last);
					root = temp;
				}
			}
			return value;
		}
		return -1;
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	public boolean rearTraversal(int value)
	{
		
		if(root == null)
		{
			log.info("no element in list");
			return false;
		}
		Node node = last;
		
		if(node.getValue() == value)
		{
			log.info("value:" +node.getValue());
			return  true;
		}
		node = node.getLeft();
		while(node != last  && node != null)
		{
			log.info("value: " +node.getValue());
			if(node.getValue() == value)
			{
				log.info("value Search: " +node.getValue());					
				return true;
			}
			node = node.getLeft();
		}
		return false;
	}
	
	/**
	 * 
	 * @param value
	 * @param direction
	 * @return boolean
	 */
	public boolean frontTraversal(int value)
	{
		if(root == null)
		{
			log.info("no element in list");
			return false;
		}
			
		Node node = root;
		
		if(node.getValue() == value)
		{
			log.info("value: " +node.getValue());
			return true;
		}
		
		node = node.getRight();
		while(node != root && node != null)
		{
			if(node.getValue() == value)
			{
				log.info("value Search: " +node.getValue());
				return true;
			}
			node = node.getRight();
		}	
		return false;
	}
		
	
	
	/**
	 * 
	 * @return
	 */
	public int getsize()
	{
		return size;
	}
	


}
