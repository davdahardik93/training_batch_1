package com.peacocktech.cirularqeue;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.TestCase;

public class QueueListTest extends TestCase{

	QueueList query = new QueueList();
	
	
	public void testrootadd()
	{
		query.addnode(23);
		System.out.println(query.rearTraversal(23));
		assertEquals(true, query.rearTraversal(23));
		
	}
	
	
	public void testadd2value() {
		query.addnode(23);
		query.addnode(45);
		query.addnode(5);
		query.addnode(3);
		assertEquals(true,query.rearTraversal(5));
	}
	
	
	public void testaddnode()
	{
		query.addnode(2);
		query.addnode(5);
		query.addnode(3);
		assertEquals(2,query.remove());
		assertNotEquals(2, query.remove());
		query.remove();
		assertNotEquals(3, query.remove());
		query.addnode(6);
	}
	
	public void testremove()
	{
		assertEquals(-1, query.remove());
		query.remove();
		query.addnode(9);
		assertEquals(9,query.remove());
		
	}
	
	public void testrootremove()
	{
		query.addnode(24);
		assertEquals(24,query.remove());
		
		query.addnode(43);
		assertNotEquals(4,query.remove());
	}
	
	public void testtraversalRear()
	{
		query.addnode(4);
		query.addnode(3);
		query.addnode(2);
		query.addnode(1);
		query.addnode(6);
		assertEquals(true,query.rearTraversal(3));
		assertEquals(false,query.rearTraversal(67));
	}
	
	public void testfailtraversalRear()
	{
		query.addnode(4);
		query.addnode(3);
		query.addnode(2);
		query.addnode(1);
		query.addnode(6);
		assertNotEquals(true,query.rearTraversal(34));
		assertNotEquals(false,query.rearTraversal(3));	
	}

	public void testfailtraversalfront(){
		query.addnode(3);
		query.addnode(2);
		query.addnode(1);
		query.addnode(6);
		assertNotEquals(true,query.frontTraversal(34));
		assertNotEquals(false,query.frontTraversal(3));	
		
	}
	
	public void testtraversalfont()
	{
		query.addnode(3);
		query.addnode(2);
		query.addnode(1);
		query.addnode(6);
		assertEquals(false,query.frontTraversal(34));
		assertEquals(true,query.frontTraversal(3));
	}
	
	public void testaddchecksize()
	{
		query.addnode(6);
		query.addnode(5);
		assertEquals(2,query.getsize());
	}

	public void testfailaddchecksize()
	{
		query.addnode(6);
		query.addnode(5);
		assertNotEquals(1, query.getsize());
	}

	
	public void testremovechecksize()
	{
		query.addnode(6);
		query.addnode(5);
		query.addnode(5);
		query.remove();
		assertEquals(2,query.getsize());
	}

	public void testfailremovechecksize()
	{
		query.addnode(6);
		query.addnode(5);
		query.remove();
		assertNotEquals(2, query.getsize());
	}	

}
