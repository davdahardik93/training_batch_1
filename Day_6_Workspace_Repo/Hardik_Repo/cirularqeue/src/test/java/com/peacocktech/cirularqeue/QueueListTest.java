package com.peacocktech.cirularqeue;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.TestCase;

public class QueueListTest extends TestCase{

	QueueList query = new QueueList();
	
	
	public void testrootadd()
	{
		query.add(23);
		System.out.println(query.traversalRear(23));
		assertEquals(true, query.traversalRear(23));
		
	}
	
	
	public void testadd2value() {
		query.add(23);
		query.add(45);
		query.add(5);
		query.add(3);
		assertEquals(true,query.traversalRear(5));
//		System.out.println(query.traversalRear(5));
	}
	
	
	public void testadd()
	{
		query.add(2);
		query.add(5);
		query.add(3);
		assertEquals(2,query.remove());
		assertNotEquals(2, query.remove());
		query.remove();
		assertNotEquals(3, query.remove());
		query.add(6);
	}
	
	public void testremove()
	{
		assertEquals(-1, query.remove());
		query.remove();
		query.add(9);
		assertEquals(9,query.remove());
		
	}
	
	public void testrootremove()
	{
		query.add(24);
		assertEquals(24,query.remove());
		
		query.add(43);
		assertNotEquals(4,query.remove());
	}
	
	public void testtraversalRear()
	{
		query.add(4);
		query.add(3);
		query.add(2);
		query.add(1);
		query.add(6);
		assertEquals(true,query.traversalRear(3));
		assertEquals(false,query.traversalRear(67));
	}
	
	public void testfailtraversalRear()
	{
		query.add(4);
		query.add(3);
		query.add(2);
		query.add(1);
		query.add(6);
		assertNotEquals(true,query.traversalRear(34));
		assertNotEquals(false,query.traversalRear(3));	
	}

	public void testfailtraversalfront(){
		query.add(3);
		query.add(2);
		query.add(1);
		query.add(6);
		assertNotEquals(true,query.traversalFront(34));
		assertNotEquals(false,query.traversalFront(3));	
		
	}
	
	public void testtraversalfont()
	{
		query.add(3);
		query.add(2);
		query.add(1);
		query.add(6);
		assertEquals(false,query.traversalFront(34));
		assertEquals(true,query.traversalFront(3));
	}
	
	public void testaddchecksize()
	{
		query.add(6);
		query.add(5);
		assertEquals(2,query.getsize());
	}

	public void testfailaddchecksize()
	{
		query.add(6);
		query.add(5);
		assertNotEquals(1, query.getsize());
	}

	
	public void testremovechecksize()
	{
		query.add(6);
		query.add(5);
		query.add(5);
		query.remove();
		assertEquals(2,query.getsize());
	}

	public void testfailremovechecksize()
	{
		query.add(6);
		query.add(5);
		query.remove();
		assertNotEquals(2, query.getsize());
	}
	

	
	

}
