
package com.peacockteck.sample;

import java.util.logging.Logger;

/**
 * This is the main class
 * 
 * @author peacock
 */
public class QueueCreation {
	private Node root;
	private Node bottom;
	private Node top;
	private int size = 0;
	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public Node getRoot() {
		return root;
	}

	public void setRoot(Node root) {
		this.root = root;
	}

	public Node getBottom() {
		return bottom;
	}

	public void setBottom(Node bottom) {
		this.bottom = bottom;
	}

	public Node getTop() {
		return top;
	}

	public void setTop(Node top) {
		this.top = top;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	/**
	 * This method is used to add element. n is reference variable of Node type
	 * 
	 * @param node
	 *            v is reference variable of int type
	 * @param v
	 */
	public void addElement(int v) 
	{
		Node node = new Node(v);

		if (root == null) 
		{
			root = node;
			root.setLeft(node);
			root.setRight(node);
			bottom = root;
			LOGGER.info("Root element is added");
		} 
		else 
		{
			root.setLeft(node);
			top.setRight(node);
			node.setLeft(top);
			node.setRight(root);
			LOGGER.info("One element is added to the queue");
		}
		top = node;
		size++;

	}

	/**
	 * This method is for delete element
	 * 
	 * @param deleteElement
	 */
	public void deleteElement() 
	{
		if (size > 1) 
		{
			root = root.getRight();
			bottom = root;
			root.setLeft(top);
			top.setRight(root);
			size--;
			LOGGER.info("One element is deleted");
		} 
		else if (size == 1) 
		{

			root = null;
			bottom = null;
			size--;
			LOGGER.info("Now queue is empty");
		} 
		else 
		{
			LOGGER.info("No operation can be performed as queue is empty");
		}
	}

	/**
	 * This is the method is for getting element
	 * 
	 * @param v
	 * @return
	 */
	public boolean getElementForward(int v) 
	{
		Node temp = bottom;
		while (temp != null && temp != top) 
		{
			if (temp.getValue() == v) 
			{
				return true;
			}
			temp = temp.getRight();
		}
		if (temp!=null && temp.getValue() == v) {
			return true;
		}
		return false;
	}// getElement
	/**
	 * @param v
	 * @return
	 */
	public boolean getElementReverse(int v) 
	{
		Node temp = top;
		while(temp != null && temp != bottom)
		{
			if(temp.getValue() == v)
			{
				return true;
			}
			temp=temp.getLeft();
		}
		if(temp!=null && temp.getValue() == v)
		{
			return true;
		}
		return false;
	}
}
