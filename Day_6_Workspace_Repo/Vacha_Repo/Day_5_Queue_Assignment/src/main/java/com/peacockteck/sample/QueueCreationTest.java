package com.peacockteck.sample;

import static org.junit.Assert.*;

import org.junit.Test;

public class QueueCreationTest {

	QueueCreation qc=new QueueCreation();
	
	boolean check;
	@Test
	public void testAddElement() 
	{
		assertEquals(0, qc.getSize());
		qc.addElement(100);
		qc.addElement(200);
		qc.addElement(300);
		assertEquals(3, qc.getSize());
		assertEquals(qc.getElementForward(100), true);
		assertEquals(qc.getElementForward(200), true);
		assertEquals(qc.getElementForward(300), true);
	}
	
	public void testFailAdd()
	{
		assertEquals(0, qc.getSize());
		qc.addElement(100);
		qc.addElement(200);
		qc.addElement(300);
		assertNotEquals(qc.getElementForward(100), false);
		assertNotEquals(qc.getElementForward(200), false);
		assertNotEquals(qc.getElementForward(300), false);
	}
	
	public void testDeleteElement()
	{
		assertEquals(0, qc.getSize());
		qc.addElement(100);
		qc.addElement(200);
		qc.addElement(300);
		assertEquals(3, qc.getSize());
		qc.deleteElement();
		qc.deleteElement();
		assertEquals(1,qc.getSize());
		assertEquals(false, qc.getElementForward(100));
		assertEquals(true, qc.getElementReverse(300));
		qc.deleteElement();
		assertEquals(false, qc.getElementForward(300));
	}
	
	public void testDeleteFail()
	{
		assertEquals(0, qc.getSize());
		qc.addElement(100);
		qc.addElement(200);
		qc.addElement(300);
		assertEquals(3, qc.getSize());
		qc.deleteElement();
		qc.deleteElement();
		assertNotEquals(false, qc.getElementForward(300));
		assertEquals(false, qc.getElementReverse(100));
		qc.deleteElement();
		assertEquals(0, qc.getSize());
	}

}
