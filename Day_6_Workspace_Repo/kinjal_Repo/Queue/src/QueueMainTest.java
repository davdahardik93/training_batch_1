import static org.junit.Assert.*;

import org.junit.Test;

public class QueueMainTest {
	QueueMain quem = new QueueMain();

	@Test
	public void testInsert() {

		quem.insert(8);
		quem.insert(13);
		quem.insert(15);
		assertEquals(2, quem.getcounter());
		quem.insert(16);
		assertNotEquals(2, quem.getcounter());
	}

	@Test
	public void testDelete() {
		quem.insert(3);
		assertNotSame(false, quem.delete());
		quem.insert(23);
		assertSame(true, quem.delete());
		quem.insert(9);
		quem.insert(34);
		assertSame(quem.delete(), true);
	}

	@Test
	public void testBackwordtraverse() {
		quem.insert(4);
		quem.insert(5);
		quem.insert(6);
		assertEquals(quem.backwordtraverse(4), true);
	}

	public void testfailBackwordtraverse() {
		quem.insert(4);
		quem.insert(5);
		quem.insert(6);
		assertNotEquals(quem.backwordtraverse(4), false);
	}

	@Test
	public void testForwardtraverse() {
		quem.insert(6);
		quem.insert(7);
		assertNotEquals(quem.forwardtraverse(6), false);

	}

}
