import java.util.logging.Logger;

class QueueMain {

	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	static QueueMain que = new QueueMain();
	Node front = null;
	Node rear = null;
	Node node;
	Node noded;
	int count = -1;
/**
 * insert method for queue
 * @param value
 * @return
 */
	int insert(int value) {
		if (front == null && rear == null) {
			node = new Node(value);
			node.value = value;
			node.left = null;
			node.right = null;
			front = node;
			rear = node;
		}

		else if (front == rear) {
			Node node1 = new Node(value);
			node1.left = front;
			node1.right = front;
			front.left = node1;
			front.right = node1;
			rear = node1;
			LOGGER.info("first node insert");
		} else {
			noded = new Node(value);
			noded.value = value;
			noded.right = front;
			noded.left = rear;
			rear.right = noded;
			if (front != null) {
				front.left = noded;
			}
			rear = noded;
			LOGGER.info("node insert");

		}
		count++;
		return value;

	}
/**
 * delete method: delete node from queue
 * @return
 */
	boolean delete() {
		if (front == null || front == rear) {
			front = null;
			rear = null;
		} else {
			front = front.right;
			front.left = rear;
			rear.right = front;
			LOGGER.info("node delete");

		}
		return true;

	}
/**
 * Backwordtraverse for queue node
 * @param value
 * @return
 */
	boolean backwordtraverse(int value) {
		Node temp = rear;
		while (temp != front) {
			if (temp.value == value) {
				LOGGER.info("traverse node found");
				return true;
			}

			temp = temp.left;

		}
		if (temp.value == value) {
			return true;
		}
		return false;
	}
	
/**
 * forwardtraverse for queue node
 * @param value
 * @return
 */

	boolean forwardtraverse(int value) {
		Node temp = front;
		while (temp != rear) {
			if (temp.value == value) {
				LOGGER.info("traverse node found");
				return true;
			}
			temp = temp.right;
		}
		if (temp.value == value) {
			return true;
		}
		return false;
	}

	int getcounter() {
		return count;
	}

}
