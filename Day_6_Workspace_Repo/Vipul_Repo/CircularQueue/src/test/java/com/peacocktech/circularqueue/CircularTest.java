package com.peacocktech.circularqueue;

import junit.framework.TestCase;

public class CircularTest extends TestCase {
	CircularQueue queue = new CircularQueue();

	public void testadd() {
		queue.addValue(20);
		queue.addValue(30);
		queue.addValue(50);
		assertEquals(true, queue.frontSearch(20));
		assertEquals(true, queue.frontSearch(30));
		assertEquals(true, queue.frontSearch(50));
		assertEquals(3, queue.getSize());

	}

	public void testaddFail() {
		queue.addValue(20);
		queue.addValue(30);
		queue.addValue(50);
		assertNotSame(false, queue.frontSearch(20));
		assertNotSame(false, queue.frontSearch(30));
		assertNotSame(2, queue.getSize());
	}

	public void testDelete() {
		queue.addValue(20);
		queue.addValue(30);
		queue.addValue(50);
		queue.deleteValue();
		assertEquals(false, queue.frontSearch(20));
		assertEquals(2, queue.getSize());
	}

	public void testDeleteFail() {
		queue.addValue(20);
		queue.addValue(30);
		queue.addValue(50);
		queue.deleteValue();
		assertNotSame(true, queue.rearSearch(20));
		assertNotSame(3, queue.getSize());
	}

	public void testFront() {
		queue.addValue(15);
		queue.addValue(18);
		queue.addValue(20);
		queue.addValue(25);
		assertEquals(true, queue.frontSearch(15));
	}

	public void testFrontFail() {
		queue.addValue(15);
		queue.addValue(23);
		assertNotSame(true, queue.frontSearch(17));
	}

	public void testRear() {
		queue.addValue(15);
		queue.addValue(18);
		queue.addValue(27);
		assertEquals(true, queue.rearSearch(18));
		assertEquals(true, queue.rearSearch(27));
	}

	public void testRearFail() {
		queue.addValue(15);
		queue.addValue(18);
		assertNotSame(true, queue.rearSearch(27));
	}
}
