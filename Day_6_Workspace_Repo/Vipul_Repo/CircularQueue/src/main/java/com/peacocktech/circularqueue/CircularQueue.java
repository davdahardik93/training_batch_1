package com.peacocktech.circularqueue;

import java.util.logging.Logger;

/**
 * Contains structure of queue node
 * 
 * @author Vipul
 *
 */
class Node {
	private int value;
	private Node left;
	private Node right;

	public Node(int value, Node left, Node right) {
		super();
		this.value = value;
		this.left = left;
		this.right = right;
	}

	public Node() {
		super();
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public Node getLeft() {
		return left;
	}

	public void setLeft(Node left) {
		this.left = left;
	}

	public Node getRight() {
		return right;
	}

	public void setRight(Node right) {
		this.right = right;
	}

}

/**
 * Contains operations on queue
 * 
 * @author Vipul
 *
 */
class CircularQueue {
	Node front;
	Node rear;
	private int size = 0;
	private final static Logger LOGGER = Logger.getLogger(CircularQueue.class.getName());

	/**
	 * Returns the size of queue
	 * 
	 * @return
	 */
	public int getSize() {
		return size;
	}

	/**
	 * Inserts the value in queue
	 * 
	 * @param value
	 */
	public void addValue(int value) {

		Node node = new Node(value, null, null);

		if (front == null && rear == null) {
			front = node;
			rear = node;
			size++;
		} else {
			node.setRight(front);
			node.setLeft(rear);
			rear.setRight(node);
			rear = node;
			if (front != null) {
				front.setLeft(rear);
			}
			size++;
		}
		LOGGER.info("added value :" + node.getValue());
	}

	/**
	 * Deletes the value from queue
	 * 
	 * @param value
	 */
	public void deleteValue() {
		Node temp = front;
		if (front == null && rear == null) {
			LOGGER.info("Queue is empty");
		} else if ((front != null) && (front.equals(rear))) {
			front = null;
			rear = null;
			size--;
		} else {
			if (temp != null) {
				front = temp.getRight();
				front.setLeft(rear);
				size--;
			}

		}
	}

	/**
	 * Searches the element from the front pointer
	 * 
	 * @param value
	 * @return boolean
	 */
	public boolean frontSearch(int value) {
		Node temp = front;
		while (temp != null && temp != rear) {
			LOGGER.info("value :" + temp.getValue());
			if (temp.getValue() == value) {
				LOGGER.info("matched value :" + temp.getValue());
				return true;
			}
			temp = temp.getRight();
		}
			if (temp != null && temp.getValue() == value) {
				LOGGER.info("last attempt matched value :" + temp.getValue());
				return true;
			}
		
		return false;
	}

	/**
	 * Searches the element from the rear pointer
	 * 
	 * @param value
	 * @return boolean
	 */
	public boolean rearSearch(int value) {
		Node temp = rear;
		while (temp != null && temp != front) {
			if (temp.getValue() == value) {
				LOGGER.info("matched value :" + temp.getValue());
				return true;
			}
			temp = temp.getLeft();
		}
		if (temp != null && temp.getValue() == value) {
			LOGGER.info("last attempt matched value :" + temp.getValue());
			return true;
		}
		return false;
	}

}
