package com.peacocktech;

import junit.framework.TestCase;

public class QueueTest 
	extends TestCase{
QueueCreation qc=new QueueCreation();
	
	boolean check;

	public void testAddElement() 
	{
		assertEquals(0, qc.getSize());
		qc.addElement(10);
		qc.addElement(11);
		qc.addElement(12);
		assertEquals(3, qc.getSize());
		assertEquals(qc.getElementForward(10), true);
		assertEquals(qc.getElementForward(11), true);
		assertEquals(qc.getElementForward(12), true);
	}
	
	public void testDeleteElement()
	{
		assertEquals(0, qc.getSize());
		qc.addElement(1);
		qc.addElement(2);
		qc.addElement(3);
		assertEquals(3, qc.getSize());
		qc.deleteElement();
		qc.deleteElement();
		assertEquals(1,qc.getSize());
		assertEquals(false, qc.getElementForward(1));
		assertEquals(true, qc.getElementReverse(3));
		qc.deleteElement();
		assertEquals(false, qc.getElementForward(3));
	}
	
	
}
