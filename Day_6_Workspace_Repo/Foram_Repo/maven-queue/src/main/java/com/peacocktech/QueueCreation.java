package com.peacocktech;

import java.util.logging.Logger;
/**
 * To create the complete Queue
 * @author peacock
 *
 */
public class QueueCreation {
	private Block root;
	private Block top, rear;
	private int size = 0;
	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public Block getRoot() {
		return root;
	}

	public void setRoot(Block root) {
		this.root = root;
	}

	public Block getTop() {
		return top;
	}

	public void setTop(Block top) {
		this.top = top;
	}

	public Block getRear() {
		return rear;
	}

	public void setRear(Block rear) {
		this.rear = rear;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
	/**
	 * Will insert the element
	 * @param v
	 */
 
	public void addElement(int v) 
	{
		Block block = new Block(v);

		if (root == null) 
		{
			root = block;
			root.setPrevious(block);
			root.setNext(block);
			rear = root;
			LOGGER.info("Root element is added");
		} 
		else 
		{
			root.setPrevious(block);
			top.setNext(block);
			block.setPrevious(top);
			block.setNext(root);
			LOGGER.info("One element is added to the queue");
		}
		top = block;
		size++;

	}
	/**
	 * will delete the element
	 */
	public void deleteElement() 
	{
		if (size > 1) 
		{
			root = root.getNext();
			rear = root;
			root.setPrevious(top);
			top.setNext(root);
			size--;
			LOGGER.info("One element is deleted");
		} 
		else if (size == 1) 
		{

			root = null;
			rear = null;
			size--;
			LOGGER.info("Now queue is empty");
		} 
		else 
		{
			LOGGER.info("No operation can be performed as queue is empty");
		}
	}
	
	public boolean getElementForward(int v) 
	{
		Block temp = rear;
		while (temp != null && temp != top) 
		{
			if (temp.getValue() == v) 
			{
				return true;
			}
			temp = temp.getNext();
		}
		if (temp!=null && temp.getValue() == v) {
			return true;
		}
		return false;
	}
	
	public boolean getElementReverse(int v) 
	{
		Block temp = top;
		while(temp != null && temp != rear)
		{
			if(temp.getValue() == v)
			{
				return true;
			}
			temp=temp.getPrevious();
		}
		if(temp!=null && temp.getValue() == v)
		{
			return true;
		}
		return false;
	}
}
