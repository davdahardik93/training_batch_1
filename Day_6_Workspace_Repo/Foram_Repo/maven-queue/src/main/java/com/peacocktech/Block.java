package com.peacocktech;

/**
 * @author Foram
 * 
 */
public class Block {
	private int value;
	private Block previous;
	private Block next;

	Block(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public Block getPrevious() {
		return previous;
	}

	public void setPrevious(Block previous) {
		this.previous = previous;
	}

	public Block getNext() {
		return next;
	}

	public void setNext(Block next) {
		this.next = next;
	}

}
