package com.peacock;

import java.util.logging.Logger;

class Node {
	int value;
	Node left;
	Node right;
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public Node getLeft() {
		return left;
	}
	public void setLeft(Node left) {
		this.left = left;
	}
	public Node getRight() {
		return right;
	}
	public void setRight(Node right) {
		this.right = right;
	}
	


	/**
	 * 
	 * @param n
	 */

	

	
}

/**
 * 
 * @author peacock
 *
 */
class CircularQueue {
	Node root;
	Node first;
	Node last;
	public int count = 0;
	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	/**
	 * 
	 * @param v
	 * @param n
	 */
	public void addNode(int v) {
		Node n = new Node();
		n.value = v;
		if (root == null) {
			root = n;
			first = root;
			last=root;
			root.setRight(n);
			root.setLeft(n);
		} else {
			root.setLeft(n);
			last.setRight(n);
			n.setLeft(last);
			n.setRight(root);
			
		}

		count++;
		last = n;
	}

	/**
	 * Delete Method
	 */
	public void deleteNode() {
  
		if (count > 1) {
			
			root = root.getRight();
			
			first = root;
			
			first.setLeft(last);
			
			last.setRight(first);
			
			
			logger.info("Node deleted");
		
		} else if (count == 1) {
			root = null;
			last = null;
			logger.info("Empty Queue"); 
			} else{
				logger.info("No operation can be done as queue is Empty");
			}
		count--;
		}
	

	/**
	 * 
	 * @param v
	 */
	/**
	 * 
	 * @param v
	 * @return boolean
	 */
	public boolean traversalForward(int v) {
		Node temp = last;
		while(temp != null && temp!= first){
			if(temp.getValue()==v)
			{
				return true;
			}
			temp=temp.getLeft();
			
		}
		if(temp!=null && temp.getValue()==v){
			return true;
		}
		return false;
		
	}
	public boolean traversalReverse(int v) {
		Node temp = first;
		while(temp != null && temp!= last){
			if(temp.getValue()==v)
			{
				return true;
			}
			temp=temp.getLeft();
			
		}
		if(temp!=null && temp.getValue()==v){
			return true;
		}
		return false;
		
	}
}
