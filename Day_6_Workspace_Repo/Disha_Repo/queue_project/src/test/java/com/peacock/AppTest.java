package com.peacock;

import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {
	CircularQueue circularQueue;
	boolean check;
	public AppTest() {
		super();
		circularQueue = new CircularQueue();
	}

	public void testAddNode() {
		
		circularQueue.addNode(100);
		assertEquals(1, circularQueue.count);
		circularQueue.addNode(200);
		circularQueue.addNode(300);
		circularQueue.addNode(400);
		circularQueue.addNode(350);
		assertEquals(5, circularQueue.count);
	}
	public void testDelete()
	{
		assertEquals(0, circularQueue.count);
		circularQueue.addNode(200);
		circularQueue.addNode(300);
		circularQueue.addNode(400);
		circularQueue.addNode(350);
		circularQueue.addNode(35);
		assertEquals(5, circularQueue.count);
		circularQueue.deleteNode();
		assertEquals(4, circularQueue.count);
	}
	public void testTraversal(){
		
		circularQueue.addNode(200);
		circularQueue.addNode(300);
		circularQueue.addNode(400);
		circularQueue.addNode(350);
		
		assertEquals(true, circularQueue.traversalForward(200));
		
	}
	
	
}
