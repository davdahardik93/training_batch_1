package com.peacock.queuedemo;

import java.util.logging.Logger;

public class QueueDemo {

	private final static Logger LOGGER = Logger.getLogger(QueueDemo.class.getName());
	Node front = null;
	Node rear = null;
	int size = 0;

	public void generate(int data) {

		Node newnode = new Node(data);
		if (size == 0) {
			front = newnode;
			rear = newnode;
			LOGGER.info("Data value added: " + newnode.getValue());
			LOGGER.info("Size: " + size);
		} else {
			newnode.setLeft(rear);
			newnode.setRight(front);
			rear.setRight(newnode);
			rear = newnode;
			front.setLeft(rear);
			LOGGER.info("Data value added: " + newnode.getValue());
			LOGGER.info("Size: " + size);
		}
		size++;
	}

	public void remove() {
		Node temp = front;

		if (front == null && rear == null) {
			LOGGER.info("Queue is empty!!");
		} else if ((front != null) && (front.equals(rear))) {
			front = null;
			rear = null;
			size--;
			LOGGER.info("Size: " + size);
		} else {
			if (temp != null) {
				front = temp.getRight();
				front.setLeft(rear);
			}
			size--;
			LOGGER.info("Size: " + size);
		}
	}

	public boolean traveseFront(int value) {
		Node temp = front;

		while (temp != rear) {

			if (temp.getValue() == value) {
				LOGGER.info("Found data:" + temp.getValue());
				return true;
			}
			temp = temp.getRight();
		}
		if (temp!=null && temp.getValue() == value) {
			LOGGER.info("Last Found data:" + temp.getValue());
			return true;
		}

		return false;
	}

	public boolean traveseReverse(int value) {
		Node temp = rear;
		while (temp != front) {

			if (temp.getValue() == value) {
				LOGGER.info("Found data:" + temp.getValue());
				return true;
			}
			temp = temp.getLeft();
		}
		if (temp!=null && temp.getValue() == value) {
			LOGGER.info("Last Found data:" + temp.getValue());
			return true;
		}

		return false;
	}

}
