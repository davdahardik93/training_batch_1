package com.peacock.queuedemo;

import junit.framework.TestCase;

public class QueueDemoTest extends TestCase {

	QueueDemo q = new QueueDemo();

	public QueueDemoTest() {
		super();
	}

	public void testAdd() {
		q.generate(10);
		assertEquals(1, q.size);
	}

	public void testFailAdd() {
		q.generate(10);
		assertNotSame(4, q.size);
	}

	public void testAddUsingTraverseRemove() {
		
		q.generate(10);
		q.generate(20);
		q.remove();
		assertEquals(false, q.traveseReverse(10));
	}

	public void testFailAddUsingTraverseRemove() {
		q.generate(10);
		q.remove();
		assertNotSame(true, q.traveseReverse(30));
	}

	public void testAddUsingTraverseFDelete() {
		q.remove();
		q.generate(10);
		assertEquals(true, q.traveseFront(10));
	}

	public void testFailAddUsingTraverseFDelete() {
		q.generate(10);
		q.remove();
		assertNotSame(true, q.traveseReverse(30));
	}

	public void testFailAddUsingTraverseR() {
		q.generate(10);
		assertEquals(false, q.traveseReverse(30));
	}

	public void testAddUsingTraverseF() {
		q.generate(10);
		assertEquals(true, q.traveseFront(10));
	}

	public void testFailAddUsingTraverseF() {
		q.generate(10);
		assertEquals(false, q.traveseFront(30));
	}

	public void testDel() {
		q.generate(10);
		q.generate(20);
		q.remove();
		assertEquals(1, q.size);
	}

	public void testFailDel() {
		q.generate(10);
		q.remove();
		assertNotSame(2, q.size);
	}

	public void testTraverseR() {
		q.generate(20);
		q.generate(30);
		q.generate(40);
		q.generate(50);
		assertEquals(false, q.traveseReverse(10));
	}

	public void testTraverseF() {
		q.generate(10);
		q.generate(20);
		q.generate(30);
		q.generate(40);
		q.generate(50);
		assertEquals(true, q.traveseFront(50));
	}

	public void testFailTraverseF() {
		q.generate(20);
		q.generate(30);
		q.generate(40);
		q.generate(50);
		assertNotSame(true, q.traveseFront(10));
	}

}
