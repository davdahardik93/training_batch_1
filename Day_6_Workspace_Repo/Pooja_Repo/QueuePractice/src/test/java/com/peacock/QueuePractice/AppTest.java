package com.peacock.QueuePractice;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
	QueueDemo d=new QueueDemo();
	
	public void testadd()
	{
		d.add(5);
		d.add(6);
		d.add(7);
		assertEquals(3,d.count());
	}
	public void testdelete()
	{
		d.add(5);
		d.add(6);
		d.add(7);
		d.delete();
		assertEquals(2,d.count());
		
	}
	public void testDelete()
	{
		d.add(5);
		d.add(6);
		d.add(7);
		d.delete();
		assertNotSame(3,d.count());
	}
	public void testForward()
	{
		d.add(5);;
		d.add(6);
		d.add(7);
		
		assertEquals(true,d.forwardSearch(6));
		
	}
	public void testBackward()
	{
		d.add(5);;
		d.add(6);
		d.add(7);
		
		assertEquals(true,d.backwardSearch(6));
	}
	
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
}
