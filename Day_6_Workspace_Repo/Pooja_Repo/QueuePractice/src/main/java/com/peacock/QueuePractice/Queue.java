package com.peacock.QueuePractice;

public class Queue {

	public Queue getNext() {
		return next;
	}

	/**
	 * constructor for setting value
	 * 
	 * @param value
	 */
	public Queue(int value) {
		this.value = value;
		next = previous = null;
	}

	public void setNext(Queue next) {
		this.next = next;
	}

	public Queue getPrevious() {
		return previous;
	}

	public void setPrevious(Queue previous) {
		this.previous = previous;
	}

	public int getValue() {
		return value;
	}

	private int value;

	Queue next, previous;
}
