package com.peacock.QueuePractice;
import java.util.logging.Logger;




public class QueueDemo {
	int count=0;
	Queue front = null, rear = null;
	private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);


	public void add(int value) {

		Queue Node = new Queue(value);
		if (front == null && rear == null) {
			
			Node.previous=Node;
			Node.next=Node;
			front = Node;
			rear = Node;
			LOGGER.info("Info Log" + Node.getValue());
		} 
		
		else {
			
			rear.next=Node;
			front.previous=Node;
			Node.previous=rear;
			Node.next=front;
			rear=Node;
			LOGGER.info("Info Log" + Node.getValue());

		
		}
		count++;
		//System.out.println("" + front.getValue());
		//System.out.println("" + rear.getValue());
		//System.out.println("" + Node.getValue());

	}
	
	public int delete ()
	{
		int value=0;
		count--;
		if(front!=null)
		{
			if (front==rear)
			{
				
				value= front.getValue();
				front=null;
				rear=null;
				LOGGER.info("Info Log" + front.getValue());
				return value;
			}
			else
			{
				value=front.getValue();
				front=front.next;
				front.previous=rear;
				rear.next=front;
				LOGGER.info("Info Log" + front.getValue());
				return value;
				
			}
		}
		
		
		return value;
		
	}
	
	public boolean forwardSearch(int value)
	{
		int value1=0,count;
		
		if(front==null)
		{
			LOGGER.info("Queue is empty");
			return false;
			
		}
		Queue temp=front;
		while(temp!=rear)
		{
			if(temp.getValue()==value)
			{
				LOGGER.info("element found"+temp.getValue());
				
				return true;
				
			}
			temp=temp.next;
		}
		if(temp.getValue() == value)
		{
			LOGGER.info("value: " +temp.getValue());
			return true;
		}
		return false;
		
		
	}
	
	public boolean backwardSearch(int value)
	{
		int value1=0,count;
				
				if(rear==null)
				{
					LOGGER.info("Queue is empty");
					return false;
					
				}
				Queue temp=rear;
				
				while(temp!=front)
				{
					if(temp.getValue()==value)
					{
						LOGGER.info("element found"+temp.getValue());
						
						return true;
						
					}
					temp=temp.previous;
				}

				if(temp.getValue() == value)
				{
					LOGGER.info("value: " +temp.getValue());
					return true;
				}
				return false;
	}
	
	
	public int count()
	{
		return count;
	}
	
}


