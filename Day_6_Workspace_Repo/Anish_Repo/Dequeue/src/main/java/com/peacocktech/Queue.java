package com.peacocktech;

import java.util.logging.Logger;

public class Queue {

	int val;							//value to enter in queue
	Queue next;							//point at next item
	Queue previous;						//point at previous item
}

class Node {
	private static final Logger LOGGER= Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	Queue front;
	Queue rear;
	private int count = 0;
	public int getCount() {
		return count;
	}

	public void add(int val) {
		if (front == null) {
			Queue q1 = new Queue();
			q1.val = val;
			q1.previous = q1;
			q1.next = q1;
			this.front = q1;
			this.rear = q1;
			count++;
			LOGGER.info("QUEUE IS INITIALIZED");
		} else {
			Queue q2 = new Queue();
			q2.val = val;
			q2.previous = rear;
			q2.next = front;
			rear.next = q2;
			front.previous = q2;
			LOGGER.info("item is added");
			rear = q2;
			count++;
		}
	}

	public void remove() {
		if(front==null){
			LOGGER.warning("Queue is empty");
		}else
		if (front.equals(rear)) {
		front=null;
		rear=null;
		count--;
		}
		else{
			LOGGER.info("item removed");
			front = front.next;
			front.previous = rear;
			rear.next = front;
			count--;
		}
	}

	public boolean frontTraverse(int val){
		Queue travel= front;
		if(travel.val != val)
		{	
			travel=travel.next;
			while(travel != front)
				{
				if(travel.val == val){
					LOGGER.info("item found");
					return true;
				}else{
					travel=travel.next;	
					}
				}
			LOGGER.info("item not found");
			return false;
		}
		LOGGER.info("found at front");
		return true;
	}
	public boolean rearTraverse(int val){
		Queue travel= rear;
		if(travel.val != val)
		{	
			travel=travel.previous;
			while(travel != rear)
				{
				if(travel.val == val){
					LOGGER.info("item found");
					return true;
				}else{
					travel=travel.previous;	
					 }
				}
			LOGGER.info("item not found");
			return false;
		}
		LOGGER.info("found at rear");
		return true;
}
}