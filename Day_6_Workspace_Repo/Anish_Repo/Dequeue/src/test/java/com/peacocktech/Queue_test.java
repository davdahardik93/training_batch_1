package com.peacocktech;

import junit.framework.TestCase;

public class Queue_test extends TestCase {
	Node n = new Node();

	public void testQueue() {
		  n.add(2);
		  n.add(3);
		  n.add(5);
		  assertEquals(n.frontTraverse(2), true);
		  assertEquals(n.frontTraverse(3), true);
		  assertEquals(n.frontTraverse(5), true);
		  assertEquals(n.getCount(), 3);
	}
	public void testaddFail() {
		  n.add(2);
		  n.add(3);
		  n.add(5);
		  assertNotSame(n.frontTraverse(2), false);
		  assertNotSame(n.rearTraverse(3), false);
		  assertNotSame(n.getCount(), 2);
	}
	public void testRemove() {
		   n.add(2);
		   n.add(3);
		   n.add(5);
		   n.remove();
		   assertEquals(n.frontTraverse(2), false); 
		   assertEquals(n.getCount(), 2);
}
	public void testRemovefail() {
		   n.add(2);
		   n.add(3);
		   n.add(5);
		   n.remove();
		   assertNotSame(n.rearTraverse(2), true); 
		   assertNotSame(n.getCount(), 3);
}
	public void testFront() {
		   n.add(15);
		   n.add(18);
		   assertEquals(n.frontTraverse(18), true);
	   }
	public void testFrontFail() {
		   n.add(15);
		   n.add(18);
		   assertNotSame(n.frontTraverse(18), false);
	   }
	public void testRear() {
		   n.add(15);
		   n.add(18);
		   n.add(23);
		   assertEquals(n.rearTraverse(18), true);
		   assertEquals(n.rearTraverse(23), true);
	   }
	public void testRearFail() {
		   n.add(15);
		   n.add(18);
		   n.add(23);
		   assertNotSame(n.rearTraverse(18), false);
		   assertNotSame(n.rearTraverse(25), true);
	   }
	public void general(){
		n.add(90);
		n.add(89);
		n.add(77);
		assertEquals(n.frontTraverse(77), true);
		assertEquals(n.rearTraverse(77), true);
		n.remove();
		assertEquals(n.rearTraverse(90), true);
		assertEquals(n.getCount(), 2);
	}
}