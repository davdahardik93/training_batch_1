package com.peacocktech.StackProgram;

import java.util.ArrayList;
import java.util.logging.*;
/**
 * This is a stack program with collection framework
 * @author Vipul Dholariya
 *
 */
public class StackProgram {
	ArrayList<String> stackEle = new ArrayList<>();
	private int tos = -1;
	private static final Logger LOGGER = Logger.getLogger(StackProgram.class.getName());

	/**
	 * This method will push the element on top of the stack.
	 * 
	 * @param name
	 */
	public void push(String name) {
		LOGGER.info("Entered in push method and entered string is : " + name);
		LOGGER.info("Top of the stack is : " + tos);
		if (name != null) {
			tos++;
			stackEle.add(name);
		} else {
			LOGGER.info("Entered string is null");
		}
		LOGGER.info("Size of the stack is : " + stackEle.size());
	}

	/**
	 * This method will pop the element from the top of the stack
	 * 
	 * @return
	 */

	public String pop() {
		String str = null;
		LOGGER.info("Entered in pop method");
		if (tos == -1) {
			LOGGER.info("Stack is empty");
		} else {
			str = stackEle.get(tos);
			LOGGER.info("popped element is : " + str);
			stackEle.remove(tos);
			tos--;
			LOGGER.info("Top of the stack is : " + tos);
		}
		LOGGER.info("Size of the stack is : " + stackEle.size());
		return str;
	}

	/**
	 * This method will return size of the stack
	 * @return
	 */
	public int sizeOfStack() {
		LOGGER.info("Entered in sizeOfStack method");
		return stackEle.size();
	}

}
