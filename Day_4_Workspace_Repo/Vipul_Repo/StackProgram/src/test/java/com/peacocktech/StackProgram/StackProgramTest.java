package com.peacocktech.StackProgram;

import junit.framework.TestCase;

public class StackProgramTest extends TestCase {
	

	StackProgram testObj = new StackProgram();

	public void testPush() {
		assertEquals(testObj.sizeOfStack(), 0);
		testObj.push("vipul");
		assertEquals(testObj.sizeOfStack(), 1);
		testObj.push("gautam");
		testObj.push("hardik");
		testObj.push(null);
		assertEquals(testObj.sizeOfStack(), 3);
		testObj.pop();
		testObj.pop();
		testObj.pop();
	}

	public void testPop() {
		testObj.push("hardik");
		assertEquals(testObj.pop(), "hardik");
		testObj.pop();
		assertEquals(testObj.sizeOfStack(), 0);
	}
}
