/**
 * 
 */
package com.peacocktech;

import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * @author peacock
 *
 */
public class Stack {

	/**
	 * @param args
	 */
	ArrayList<String> al = new ArrayList<String>();
	int count = 0;
	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public void push(String s) {

		try {
			if (s != null) {
				logger.info("Adding item " + s);
				al.add(s);
				count++;
			}
		} catch (Exception e) {
			logger.warning("can't add item " + s);
		}

	}

	public String getTop() {
		return al.get(al.size() - 1);
	}

	public int getCount() {
		return al.size();
	}

	public String pop() {
		try {
			if (count != 0) {
				logger.info("Extracting item "+ al.get(al.size() - 1));
				count--;
				al.remove(al.size() - 1);
				

			}
		} catch (Exception e) {
			logger.warning("There is no item to display");
			System.out.println(e);
		}
		return al.remove(al.size() - 1);
	}



}
