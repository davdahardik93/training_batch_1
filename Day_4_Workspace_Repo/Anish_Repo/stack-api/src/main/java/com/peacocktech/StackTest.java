package com.peacocktech;

import static org.junit.Assert.*;

import org.junit.Test;

public class StackTest {
	Stack sd = new Stack();

	@Test
	public void testPush() {

		sd.push("Anish");
		assertEquals("Anish", sd.getTop());
		sd.push("sujit");
		sd.push("ruchit");
		assertEquals(3, sd.getCount());
		sd.push("Anish");
		sd.push(null);
		assertEquals(null, sd.getTop());
	}
	@Test
	public void testPop() {
		sd.push("sujit");

		assertEquals("sujit", sd.pop());
		sd.pop();
	}
}
