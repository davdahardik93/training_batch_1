package com.peacock.stackdemo;

import org.junit.Test;

import com.peacock.stackdemo.Stack;

import static org.junit.Assert.*;

import java.util.LinkedList;


public class StackTest {

	Stack stack = new Stack();
	@Test
	public void testpush() {

		assertEquals("Ground ",stack.top(),0);
		stack.push("shweta");
		stack.push("hello");
		assertEquals("total element push",stack.top(),2);
		

	}

	@Test
	public void testpop() {
		String value = "xyz";
		stack.push(value);
		assertEquals("total element push",stack.top(),1);
		stack.push(value);
		assertEquals("value push is equal or not",stack.pop(),value);
		assertEquals("after pop operation",stack.top(),1);
		stack.push("hello 1233");		
		
		assertEquals(stack.top(),2);
	}
}
