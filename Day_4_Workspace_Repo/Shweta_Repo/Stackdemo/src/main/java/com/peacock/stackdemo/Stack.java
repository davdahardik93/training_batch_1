package com.peacock.stackdemo;

import java.util.ArrayList;

import java.util.logging.Logger;

/**
 * this is class
 * @param str
 */
 public class Stack {

	final Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	ArrayList<String> stackArray=new ArrayList<String>();
	/**
	 * this method is to push the element
	 * @param str
	 */
	public void push(String str)
	{
		 

		if(str != null)
		{
			stackArray.add(str);
			logger.warning("element push: " +str);
		}
		else
			logger.warning("element push: is null ");
		
	}
	
	/**
	 * this method is to pop the element
	 * @param str
	 * @return str
	 */
	public String pop()
	{
		String str = "";
		int size =stackArray.size()-1;
		logger.warning("element pop: " +stackArray.get(size));
		if(size != 0)
		{
			str = stackArray.get(size);
			stackArray.remove(size);
		}
		return str;
			
		
	}
	/**
	 * this method is to top the element
	 * @param str
	 * @return stackArray.size();
	 */
	
	public int top()
	{
		return stackArray.size();
	}
	 
}
