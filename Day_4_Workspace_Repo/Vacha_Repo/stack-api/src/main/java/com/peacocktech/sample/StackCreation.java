package com.peacocktech.sample;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * This class contains stack operation
 * @author peacock
 *
 */
public class StackCreation 
{
	private static final Logger LOGGER=Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private int top=-1;
	private int i;
	private ArrayList<String> al=new ArrayList<String>();
	
	Scanner sc=new Scanner(System.in);
	
	public int getTop()
	{
		return top;
	}
	/**
	 * str is String type variable to insert String in stack. 
	 * @param str
	 */
	
	public void pushElement(String str)
	{
		if(str!=null)
		{
		al.add(str);
		top++;
		
		for(i=0;i<top;i++)
		{
			/**
			 * It is method of List to get perticular object 
			 * @param get
			 */
			LOGGER.info(al.get(i));
		}
		}
	}
	/**
	 * for delete element
	 * @param popElement
	 */
	public void popElement()
	{
		if(al!=null)
		{
			
			LOGGER.info("value of top:"+top);
			
			al.remove(al.get(top));
			
			
			for(i=0;i<top;i++)
			{
				if(top<0)
				{
					
					LOGGER.info("Stack is Empty");
				}
				
				LOGGER.info("My string is:"+al.get(i));
			}
			top--;
			LOGGER.info("After pop operation:"+top);
		}
		else
		{
			LOGGER.info("Stack is empty");
		}
	}
}
