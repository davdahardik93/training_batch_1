package com.peacocktech.sample;

import static org.junit.Assert.*;

import java.util.logging.Logger;

import org.junit.Test;

import junit.framework.TestCase;

public class StackCreationTest extends TestCase{

	//private final static Logger LOGGER=Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	StackCreation sc=new StackCreation();
	@Test
	public void testPushElement() 
	{
		assertEquals(-1, sc.getTop());
		sc.pushElement("Vacha");
		sc.pushElement("Aayush");
		
		sc.pushElement(null);
		assertEquals(1, sc.getTop());
	}

	@Test
	public void testPopElement() 
	{
		assertEquals(-1, sc.getTop());
		sc.pushElement("Rasagna");
		sc.pushElement("Jignesh");
		assertEquals(1, sc.getTop());
		sc.popElement();
		assertEquals(0, sc.getTop());
	}

}
