package com.peacocktech.stackdemo;

import java.util.ArrayList;
import java.util.logging.Logger;
/**
 * SimpleStack class
 * @author peacock
 *
 */
public class SimpleStack {
	static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	ArrayList<String> arraylist = new ArrayList<String>();

	/**
	 * This is push method
	 * @param str
	 */
	public void push(String str) {
		if (str != null) {
			arraylist.add(str);
		} else {

			logger.info("not valid"); 
		}
	}
 /**
  * Pop will delete element
  * @return
  */

	public String pop() {
		int size = arraylist.size() - 1;
		String str="";
		if (size != -1){
			str=arraylist.get(size);
			
			arraylist.remove(size);
			
		}
	return str;

	}
	/**
	 * Top will return top
	 * @return
	 */
	public int top()
	{
		return arraylist.size();
	}

	
/**
 * main method
 * @param args
 */
	public static void main(String[] args) {

		SimpleStack sk = new SimpleStack();

		
		sk.push("sujit");
		sk.push("yash");
		logger.info(sk.pop()); 
		
		sk.push("hiii");
		logger.info(sk.pop()); 
		  
		
		
		
	}

}
