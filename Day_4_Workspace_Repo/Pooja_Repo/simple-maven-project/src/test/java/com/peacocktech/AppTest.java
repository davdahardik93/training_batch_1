package com.peacocktech;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    String value="Hello";
    public void testpush()
    {
App D = new App();
        
        D.push("Pooja");
        D.push("Krishna");
        D.push("Kinjal");
        D.push("Tina");
        D.push("Heta");
        
        assertEquals("Heta", D.pop());
        assertEquals("Kinjal", D.pop());
        assertEquals(value, D.pop());
        
        
        D.pop();
        
    }
        
    
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
}
