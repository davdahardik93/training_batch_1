package com.peacock;

import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase{
	public AppTest(String StackTest)
	{
		super(StackTest);
	}
	public void testPush(){
		App a = new App();
		a.push("Disha");
//		a.Pop();
		assertEquals("Disha", a.pop());
	}
	public void testPop(){
		App a = new App();
		a.push("Gautam");
		a.push("Disha");
		a.pop();
		assertEquals(a.currentCount(),0);
	}
}
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
   /* public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
   /* public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
   /* public void testApp()
    {
        assertTrue( true );
    }
}
*/