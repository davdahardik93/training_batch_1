package com.peacock;

import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Hello world!
 *
 */
public class App 
{
	Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	int count=-1;
	ArrayList <String> arr = new ArrayList <String>();
 
    
    /**
     * 
     * @param parameter
     */
    public void push(String parameter){
		String par;
		par=parameter;
		arr.add(par);
		count++;
		logger.info("Name is "+arr);
	}
    /**
     * 
     * @return str
     */
    public String pop()
    {
    	String str = arr.get(count);
    	logger.info(str);
    	arr.remove(count);
    	count--;
    	
    	return str;
    }
    /**
     * 
     * @return count
     */
    public int currentCount(){
    	
    	return count;
    	
    }
   
}

