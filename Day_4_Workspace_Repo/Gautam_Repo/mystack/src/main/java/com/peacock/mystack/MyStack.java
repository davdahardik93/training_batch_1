package com.peacock.mystack;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * gautam vanani
 * 
 * @author peacock
 *
 */
public class MyStack {
	public static final String TAG = MyStack.class.getName();
	protected int top = -1;

	protected ArrayList<String> arrayList = new ArrayList<String>();
	private static Logger logger = Logger.getLogger(TAG);

	public int getTop() {
		return top;
	}

	/**
	 * method to push data in stack
	 * 
	 * @param strData
	 *            data to add
	 */
	protected void push(String strData) {
		if (null != strData) {
			arrayList.add(strData);
			top++;
			printLog("push at " + top + "-" + strData);
		} else {
			printLog("null value not allowed");
		}
	}

	/**
	 * method to get object at top of the stack
	 * 
	 * @return data at top
	 */
	protected String pop() {
		if (top != -1) {
			String strData = arrayList.get(top);
			printLog("pop from " + top + "-" + strData);
			top--;
			return strData;
		}
		return null;
	}

	/**
	 * method to print log
	 * 
	 * @param msg
	 *            message to log as level info
	 */
	public void printLog(String msg) {
		logger.log(Level.INFO, msg);
	}

}