package com.peacock.mystack;

import junit.framework.TestCase;

/**
 * gautam vanani
 * 
 * @author peacock
 */
public class MyStackTest extends TestCase {
	public MyStackTest(String name) {
		super(name);
	}

	String value1 = "Gautam";
	String value2 = "peacock";
	MyStack myStack = new MyStack();

	// public void testPush() {
	//
	// myStack.push(value);
	//
	// assertEquals(value, myStack.pop());
	// }
	public void testFailPush() {

		myStack.push(value1);
		assertNotSame("value", myStack.pop());
	}

	public void testFailPop() {

		myStack.push(value1);
		myStack.push(value1);
		assertNotSame(value2, myStack.pop());
	}

	public void testPush() {

		assertEquals(myStack.getTop(), -1);

		myStack.push("gautam");
		myStack.push("peacock");

		assertEquals(myStack.getTop(), 1);
	}

	public void testPop() {
		myStack.push("developer");
		myStack.push(null);
		myStack.push(null);
		myStack.push(null);

		// assertEquals(myStack.getTop(), 1);

		assertEquals(myStack.pop(), "developer");
	}

}
