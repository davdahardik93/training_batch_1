package com.peacocktech;

import junit.framework.TestCase;


/**
 * Unit test for simple App.
 */
public class StackTest 
    extends TestCase 
{
	 StackDemo sd = new StackDemo();
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    
    public void testPush()
    {
        assertEquals(1, sd.getTop());
        sd.push("Foram");
        sd.push("Vacha");
        sd.push("Sweta");
        sd.display();
        assertEquals(4, sd.getTop());
        sd.push(null);
        assertEquals(4, sd.getTop());
        tearDown();
    }
    public void testPop()
    {
    	assertEquals(1, sd.getTop());
    	sd.push("Hello");
    	sd.pop();
    	assertEquals(1, sd.getTop());
    	tearDown();
    }
    public void tearDown(){
    	sd.setTop(1);
    }
    
}
