package com.peacocktech;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Logger;

/**
 * StackDemo is the main class
 * 
 * @author peacock
 *
 */

public class StackDemo {

	private int top = 1;
	String str2;
	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	ArrayList<String> al = new ArrayList<String>();

	/**
	 * push method for insert
	 * 
	 * @param str
	 */
	public void push(String str) {
		if (str != null) {
			al.add(str);
			top++;
		}

	}

	/**
	 * pop method for deletion
	 */
	public void pop() {

		if (top == 0)
			LOGGER.info("Stack is empty");

		else {
			
			str2 = al.remove(al.size() - 1);
			LOGGER.info("Popped element is: " + str2);
			top--;
		}

	}

	/**
	 * display method to see the elements
	 */
	public void display() {
		LOGGER.info("The elements in stack are");
		
		Iterator<String> itr = al.iterator();
		
		while (itr.hasNext()) {
			LOGGER.info(itr.next());
		}
	}

	public int getTop() {
		return top;
	}

	public void setTop(int a) {
		top = a;
	}

}
