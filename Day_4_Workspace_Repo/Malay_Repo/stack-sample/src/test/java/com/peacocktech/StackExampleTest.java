package com.peacocktech;

import junit.framework.TestCase;

public class StackExampleTest extends TestCase {

	StackExample stack = new StackExample();
	
	public void testPush() {
		//fail("Not yet implemented");
		assertEquals(stack.size(),0);
		stack.push("Malay");
		
		assertEquals(stack.size(), 1);
		
		stack.push("Jay");
		stack.push("Anish");
		stack.push("Gautam");
		
		assertEquals(stack.size(), 4);
		stack.push(null);
		
	}

	public void testPop() {
		
		assertEquals(stack.size(),0);
		
		stack.push("Jay");
		stack.push("Gautam");
		assertEquals(stack.size(), 2);
		
		assertEquals(stack.pop(), "Gautam");
		assertEquals(stack.size(), 1);
		
		
	}

}
