package com.peacocktech;

import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * 
 * @author Peacock
 *
 */
public class StackExample {

	Logger log = Logger.getLogger(com.peacocktech.StackExample.class.getName());

	ArrayList<String> stack = new ArrayList<String>();

	int stackTop = -1;

	/**
	 * 
	 * @param str
	 */
	public void push(String str) {
		if(str!= null){
			stack.add(str);
			stackTop++;
			log.info("You pushed " + str);
		}
		else
		{
			log.info("Do not enter NULL value");
		}
	}

	/**
	 * 
	 * @return
	 */
	public String pop() {
		String popped = stack.get(stackTop);
		if (stackTop == -1) {
			log.info("Stack is empty");
		} else {
			stack.remove(stackTop);

		}
		log.info("Stack Popped " + popped);
		return popped;
	}

	/**
	 * 
	 * @return
	 */
	public int size() {
		return stack.size();
	}

}
