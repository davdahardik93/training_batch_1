package com.peacocktech.com.peacocktech.StackDemo;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import peacocktech.App;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {
	App app = new App();

	/**
	 * Create the test case
	 *
	 * @param testName
	 *            name of the test case
	 */
	public AppTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(AppTest.class);
	}

	public void testPush() {

		app.push("Gautam");
		app.push("Malay");
	}

	public void testPop() {
		app.push("Gautam");
		app.push("Malay");
		assertEquals(app.pop(), "Malay");

	}

	/**
	 * Rigourous Test :-)
	 */
	public void testApp() {
		assertTrue(true);
		// assertEquals(, "jay");
	}
}
