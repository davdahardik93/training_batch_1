package peacocktech;

import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Hello world!
 *
 */
public class App {
	ArrayList<String> stack = new ArrayList<String>();
	Logger log = Logger.getLogger(App.class.getName());

	/**
	 * push method for inserting values
	 * 
	 * @param name
	 */

	public void push(String name) {
		stack.add(name);
		log.info("Push is :");
		log.info(stack.toString());
		log.info("Size is :" + stack.size());
	}

	/**
	 * pop method for inserting values
	 * @return pop =1 means its done -- success
	 **/
	public String pop() {
		int size = stack.size() - 1;
		String returnname= stack.remove(size);
		log.info("After Pop is :");
		log.info("Size is :" + stack.size());
		log.info(stack.toString());
		return returnname;
		
	}

	/**
	 * main method
	 * 
	 * @param args
	 */
//	public static void main(String[] args) {
//		App a = new App();
//		a.push("Jay");
//		a.push("Yash");
//		a.push("Shweta");
//		a.push("Foram");
//		a.push("Gautam");
//		a.pop();
//
//	}
}
