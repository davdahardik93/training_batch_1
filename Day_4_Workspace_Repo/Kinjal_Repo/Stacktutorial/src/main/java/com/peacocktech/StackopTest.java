package com.peacocktech;
import junit.framework.TestCase;

import java.util.logging.Logger;

import org.junit.Test;

public class StackopTest extends TestCase {

	 private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	 
	@Test
	public void testPush() {
			Stackop stkop=new Stackop();
			stkop.push("riya");
			stkop.push("jiya");
			stkop.push(null);
			
			assertEquals(1, stkop.getSize());
		
	}

	@Test
	public void testPop() {
		String st;
		Stackop stkop=new Stackop();
		stkop.push("rr");
		assertEquals(0, stkop.getSize());
		st=stkop.pop();
		 LOGGER.info(st);
		 LOGGER.warning(st);
		assertEquals(-1, stkop.getSize());
		
		}

}
