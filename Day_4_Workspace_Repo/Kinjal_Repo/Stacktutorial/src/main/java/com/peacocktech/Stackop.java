package com.peacocktech;
import java.util.ArrayList;

/**
 * This class is contain push and pop operation of stack.
 * @author peacock
 *
 */

public class Stackop {
	
	 static ArrayList<String> a1=new ArrayList<String>();
	int count=-1;
	/**
	 * this method is used for push operation of stack.
	 * @param s
	 */
	 public void push(String s)
	 {
		if(s!= null){
	     a1.add(s);
	     count++;
	     
		}
		
	 }
      /**
       * this method is used for pop operation of stack.
       * @return
       */
	 public String pop()
	 {
		 String str="";
		 if(count != (-1))
		 {
			 
			  str=a1.get(count);
			  a1.remove(count);
			  count--;
			  
			 
		 }
		 return str;
		
	 }
	 
	public int getSize()
	{
		return count;
	}
	
	
	}

