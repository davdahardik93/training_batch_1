/**
 * 
 */
package com.peacocktech;

import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * @author peacock
 *
 */
public class demo {

	/**
	 * @param args
	 */
	ArrayList<String> al = new ArrayList<String>();
	int count = 0;
	private final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	void push(String s) {

		try {
			if (s != null) {
				logger.info(s + "is added.");
				System.out.println("Pushed item is " + s);
				al.add(s);
				count++;
			}
		} catch (Exception e) {
			logger.warning("can't add item"+ s);
			System.out.println(e);
		}

	}

	public String getTop() {
		return al.get(al.size() - 1);
	}

	public String pop() {
		try {
			if (count != 0) {
				logger.info("Extracting item");
				count--;
				System.out.println("Pop " + al.get(al.size() - 1) + " from stack");

			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return al.remove(al.size() - 1);
	}

	public static void main(String[] args) {
		demo d = new demo();
		d.push("hardik");
		d.push("shweta");
		d.push("anish");
		d.push("vacha");
		d.pop();

	}

}