package com.peacocktech;

import junit.framework.TestCase;

public class demoTest extends TestCase {
	demo d = new demo();

	public void testPush() {
		d.push("PQR");
		assertEquals("PQR", d.getTop());
		d.push("ABCD");
		assertEquals("ABCD", d.getTop());
		d.push("XYZ");
		assertEquals("XYZ", d.getTop());
		d.push("MNO");
		assertEquals("MNO", d.getTop());
		d.push("");
		assertEquals("", d.getTop());
		d.push("EMPTY");
		assertEquals("EMPTY", d.getTop());
	}

	public void testPop() {
		d.push("ABCD");
		assertEquals("ABCD", d.pop());
		d.pop();

	}

}
