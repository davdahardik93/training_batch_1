package com.peacocktech.tree;

import junit.framework.TestCase;

public class TreeTestCase extends TestCase {
	SampleTree st=new SampleTree();
	public void testRoot() {
		st.setroot(10);
		assertEquals(10, st.rootvalue);
	}
	public void testcreatechild(){
		st.setroot(50);
		st.setchild(100, 50, 'l');
		assertEquals(2,st.treeSize());
	}
	public void testmultiplechild(){
		st.setroot(60);
		st.setchild(30, 60, 'l');
		st.setchild(50, 60, 'r');
		st.setchild(20, 50, 'r');
		
		assertEquals(4, st.treeSize());
		st.depthSearch();
//		st.getElements();
	}
	
	public void testchild()
	{
		st.setroot(60);
		st.setchild(30, 60, 'l');
		st.setchild(50, 60, 'r');
		st.setchild(20, 50, 'r');
		
		assertEquals(4, st.treeSize());	
		st.breadthSearch();
		st.getElements();
	}
}

