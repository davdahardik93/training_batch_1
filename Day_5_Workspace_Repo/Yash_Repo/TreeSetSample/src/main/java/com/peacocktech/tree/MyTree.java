package com.peacocktech.tree;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.logging.Logger;

/**
 * class tree node
 * @author peacock
 *
 */
public class MyTree {
	private int value;
	private MyTree left;
	private MyTree right;

	/**
	 * constructor
	 * @param value
	 * @param left
	 * @param right
	 */
		public MyTree(int value, MyTree left, MyTree right) {
			this.left = left;
			this.right = right;
			this.value = value;
		}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public MyTree getLeft() {
		return left;
	}

	public void setLeft(MyTree left) {
		this.left = left;
	}

	public MyTree getRight() {
		return right;
	}

	public void setRight(MyTree right) {
		this.right = right;
	}

}

class SampleTree {
	MyTree node;
	MyTree root;
	int rootvalue;
	private static final Logger LOGGER = Logger.getLogger(SampleTree.class.getName());

	Map<Integer, MyTree> tree = new HashMap<Integer, MyTree>();
/**
 * set root
 * @param value
 */
	public void setroot(int value) {
		root = new MyTree(value, null, null);
		tree.put(value, node);
		rootvalue = value;
		

	}
/**
 * set child
 * @param value
 * @param parentvalue
 * @param direction
 */
	public void setchild(int value, int parentvalue, char direction) {
		node = new MyTree(value, null, null);
		tree.put(new Integer(node.getValue()), node);
		setnode(node, parentvalue, direction);

	}
/**
 * set node
 * @param node
 * @param parentvalue
 * @param direction
 */
	public void setnode(MyTree node, int parentvalue, char direction) {
		if (direction == 'l') {
			MyTree parent =  tree.get(parentvalue);
			
			if (parent!=null && parent.getLeft() == null) {
				parent.setLeft(node);
			} else {
				LOGGER.info("wrong");
			}
		} else if (direction == 'r') {
			MyTree parent1 =  tree.get(parentvalue);
			if (parent1!=null &&parent1.getRight() == null) {
				parent1.setRight(node);

			} else {
				LOGGER.info("wrong");
			}

		}
	}
	/**
	 * depth method
	 */
	public void depthSearch() {
		dfs(root);
	}
/**
 * depth implement
 * @param root
 */
	public void dfs(MyTree root) {
		if (root != null) {
			LOGGER.info("value : " + root.getValue());
		}
		if (root != null && root.getLeft() != null) {
			dfs(root.getLeft());
		}
		if (root != null && root.getRight() != null) {
			dfs(root.getRight());
		}
	}
/**
 * breath method
 */

	public void breadthSearch() {
		bfs(root);
	}
/**
 * depth method
 * @param root
 */
	public void bfs(MyTree root) {
		Queue<MyTree> queue = new LinkedList<MyTree>();
		queue.add(root);
		bfsRec(queue);
	}

/**
 * implement bfs
 * @param q
 */
	public void bfsRec(Queue<MyTree> q) {
		if (q.isEmpty()) {
			return;
		}
		MyTree chilNode = q.poll();
		if (chilNode != null) {
			LOGGER.info("value : " + chilNode.getValue());
		}
		if (chilNode != null && chilNode.getLeft() != null) {
			q.add(chilNode.getLeft());
		}
		if (chilNode != null && chilNode.getRight() != null) {
			q.add(chilNode.getRight());
		}
		bfsRec(q);
	}

	public void getElements() {
		for (Integer key : tree.keySet()) {
			LOGGER.info("keys are :" + key);
		}
	}
/**
 * return size
 * @return
 */
	public int treeSize() {
		return tree.size();
	}
	
}
