package com.peacocktech;

import java.util.*;
import java.util.logging.Logger;


class Node {
	int value;
	Node left;
	Node right;

	public Node(int value, Node left, Node right) {
		super();
		this.value = value;
		this.left = left;
		this.right = right;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public Node getLeft() {
		return left;
	}

	public void setLeft(Node left) {
		this.left = left;
	}

	public Node getRight() {
		return right;
	}

	public void setRight(Node right) {
		this.right = right;
	}
}


public class Tree {
	Node root;
	Node node;
	int rootValue;
	private static final Logger LOGGER = Logger.getLogger(Tree.class.getName());

	Map<Integer, Node> treeMap = new HashMap<>();
	public void setRoot(int input) {
		root = new Node(input, null, null);
		treeMap.put(input, root);
		LOGGER.info("Size of tree after addding root:" + treeMap.size());
		rootValue = input;
	}
	public void createNode(int input, int parentValue, char dir) {
		node = new Node(input, null, null);
		treeMap.put(input, node);
		setLink(node, parentValue, dir);
	}
	public void setLink(Node node, int parentValue, char dir) {
		if (dir == 'l') {
			Node parent = treeMap.get(parentValue);
			if (parent.getLeft() == null) {
				parent.setLeft(node);
			} else {
				LOGGER.info("Left node position is full");
			}
		}
		if (dir == 'r') {
			Node parent = treeMap.get(parentValue);
			if (parent.getRight() == null) {
				parent.setRight(node);
			} else {
				LOGGER.info("Right node position is full");
			}
		}
	}
	public void depthSearch() {
		dfs(root);
	}
	public void dfs(Node root) {
		if (root != null) {
			LOGGER.info("value : " + root.getValue());
		}
		if (root != null && root.getLeft() != null) {
			dfs(root.getLeft());
		}
		if (root != null && root.getRight() != null) {
			dfs(root.getRight());
		}
	}
	public void breadthSearch() {
		bfs(root);
	}
	public void bfs(Node root) {
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		bfsRec(queue);
	}
	public void bfsRec(Queue<Node> q) {
		if (q.isEmpty()) {
			return;
		}
		Node chilNode = q.poll();
		if (chilNode != null) {
			LOGGER.info("value : " + chilNode.getValue());
		}
		if (chilNode != null && chilNode.getLeft() != null) {
			q.add(chilNode.getLeft());
		}
		if (chilNode != null && chilNode.getRight() != null) {
			q.add(chilNode.getRight());
		}
		bfsRec(q);
	}
	public void getElements() {
		for (Integer key : treeMap.keySet()) {
			LOGGER.info("keys are :" + key);
		}
	}
	public int sizeOfTree() {
		return treeMap.size();
	}
}