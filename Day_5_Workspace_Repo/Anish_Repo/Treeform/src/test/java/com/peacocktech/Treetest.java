package com.peacocktech;

import junit.framework.TestCase;

public class Treetest extends TestCase {
	Tree tree = new Tree();

	public void testRoot() {
		tree.setRoot(20);
		assertEquals(20, tree.rootValue);
	}

	public void testChildNode() {
		tree.setRoot(20);
		tree.createNode(30, 20, 'l');
		tree.createNode(50, 20, 'r');
		assertEquals(3, tree.sizeOfTree());
		tree.getElements();// This will print all the elements of tree in
							// console
	}

	public void testDFS() {
		tree.setRoot(20);
		assertEquals(20, tree.rootValue);
		tree.createNode(30, 20, 'l');
		tree.createNode(50, 20, 'r');
		tree.createNode(60, 50, 'r');
		tree.createNode(70, 30, 'l');
		tree.createNode(80, 50, 'l');
		tree.createNode(90, 30, 'r');
		tree.createNode(100, 70, 'l');
		tree.createNode(110, 100, 'l');
		tree.createNode(40, 80, 'r');
		assertEquals(10, tree.sizeOfTree());
		tree.depthSearch();
		tree.getElements();// This will print all the elements of tree in
		// console

	}

	public void testBFS() {
		tree.setRoot(20);
		assertEquals(20, tree.rootValue);
		tree.createNode(30, 20, 'l');
		tree.createNode(50, 20, 'r');
		tree.createNode(60, 50, 'r');
		tree.createNode(70, 30, 'l');
		tree.createNode(80, 50, 'l');
		tree.createNode(90, 30, 'r');
		tree.createNode(100, 70, 'l');
		tree.createNode(110, 100, 'l');
		tree.createNode(40, 80, 'r');
		assertEquals(10, tree.sizeOfTree());
		tree.breadthSearch();
		tree.getElements();// This will print all the elements of tree in
		// console
	}
}
