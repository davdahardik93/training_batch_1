package com.peacocktech.treeprogram;

import java.util.*;
import java.util.logging.Logger;

/**
 * Structure of individual node of tree
 * 
 * @author Vipul
 *
 */
class Node {
	int value;
	Node left;
	Node right;

	public Node(int value, Node left, Node right) {
		super();
		this.value = value;
		this.left = left;
		this.right = right;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public Node getLeft() {
		return left;
	}

	public void setLeft(Node left) {
		this.left = left;
	}

	public Node getRight() {
		return right;
	}

	public void setRight(Node right) {
		this.right = right;
	}
}

/**
 * Contains operations on tree
 * 
 * @author Vipul
 *
 */
public class TreeProgram {
	Node root;
	Node node;
	int rootValue;
	private static final Logger LOGGER = Logger.getLogger(TreeProgram.class.getName());

	Map<Integer, Node> treeMap = new HashMap<>();

	/**
	 * It sets the root element in tree
	 * 
	 * @param input
	 *            - Value which becomes root of the tree
	 */
	public void setRoot(int input) {
		root = new Node(input, null, null);
		treeMap.put(input, root);
		LOGGER.info("Size after addding root:" + treeMap.size());
		rootValue = input;
	}

	/**
	 * It creates the node of tree
	 * 
	 * @param input
	 *            - Value which will be inserted in tree
	 * @param parentValue
	 *            - Value which specifies parent of current inserting node
	 * @param dir
	 *            - Direction from parent, where child node will inserted
	 */
	public void createNode(int input, int parentValue, char dir) {
		node = new Node(input, null, null);
		treeMap.put(input, node);
		setLink(node, parentValue, dir);
	}

	/**
	 * Sets link between parent and child node
	 * 
	 * @param node
	 *            - Object of child node
	 * @param parentValue
	 *            - Value which specifies parent of current inserting node
	 * @param dir
	 *            - Direction from parent, where child node will inserted
	 */
	public void setLink(Node node, int parentValue, char dir) {
		if (dir == 'l') {
			Node parent = treeMap.get(parentValue);
			if (parent.getLeft() == null) {
				parent.setLeft(node);
			} else {
				LOGGER.info("Node is already available at this position");
			}
		}
		if (dir == 'r') {
			Node parent = treeMap.get(parentValue);
			if (parent.getRight() == null) {
				parent.setRight(node);
			} else {
				LOGGER.info("Node is already available at this position");
			}
		}
	}

	/**
	 * It is used for Depth First Search traversal
	 */
	public void depthSearch() {
		dfs(root);
	}

	/**
	 * It starts the DFS from root node
	 * 
	 * @param root
	 *            - root node
	 */
	public void dfs(Node root) {
		if (root != null) {
			LOGGER.info("value : " + root.getValue());
		}
		if (root != null && root.getLeft() != null) {
			dfs(root.getLeft());
		}
		if (root != null && root.getRight() != null) {
			dfs(root.getRight());
		}
	}

	/**
	 * It is used for Breadth First Search traversal
	 */
	public void breadthSearch() {
		bfs(root);
	}

	/**
	 * It is used to add root node in queue
	 * 
	 * @param root
	 *            - root node
	 */
	public void bfs(Node root) {
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		bfsRec(queue);
	}

	/**
	 * It starts BFS from root node
	 * 
	 * @param q
	 *            - Queue which will contain nodes while traversing
	 */
	public void bfsRec(Queue<Node> q) {
		if (q.isEmpty()) {
			return;
		}
		Node chilNode = q.poll();
		if (chilNode != null) {
			LOGGER.info("value : " + chilNode.getValue());
		}
		if (chilNode != null && chilNode.getLeft() != null) {
			q.add(chilNode.getLeft());
		}
		if (chilNode != null && chilNode.getRight() != null) {
			q.add(chilNode.getRight());
		}
		bfsRec(q);
	}

	/**
	 * It prints all the elements presented in tree
	 */
	public void getElements() {
		for (Integer key : treeMap.keySet()) {
			LOGGER.info("keys are :" + key);
		}
	}

	/**
	 * It returns the size of tree
	 * 
	 * @return size of tree
	 */
	public int sizeOfTree() {
		return treeMap.size();
	}
}
