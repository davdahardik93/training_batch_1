package peacotreedemo;

public class Treedemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Tree tree = new Tree();
		Node root = new Node(3);
		tree.addroot(root);
		Node node= new Node(6);
		tree.addnode("left", root, node);
		Node node1= new Node(7);
		tree.addnode("right",root, node1);
		Node node2= new Node(16);
		tree.addnode("right", node, node2);
		Node node3= new Node(12);
		tree.addnode("left", node, node3);
		tree.dfs(root);
		
	}

}
