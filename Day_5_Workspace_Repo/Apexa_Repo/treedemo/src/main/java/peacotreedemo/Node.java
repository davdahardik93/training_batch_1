package peacotreedemo;

/**
 * class node represent the node of tree
 * @author peacock
 *
 */
public class Node{
	
	private int value;
	Node right;
	Node left;
	/**
	 * set value to zero and children of node to null
	 */
	public Node(){
		value = 0;
		right = null;
		left = null;
	}
	public Node(int val){
		value = val;
		right = null;
		left = null;
	}
	public int getValue()
	{
		return value;
	}
	
}