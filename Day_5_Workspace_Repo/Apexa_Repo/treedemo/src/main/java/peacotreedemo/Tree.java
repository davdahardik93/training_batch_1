package peacotreedemo;

import java.util.HashMap;
import java.util.logging.Logger;

/**
 * class for representing tree
 * 
 * @author peacock
 *
 */

public class Tree extends Node {
	
	HashMap<Integer, Node> hmtree = new HashMap<Integer, Node>();
	private static final Logger LOGGER = Logger.getLogger(Tree.class.getName());
	/**
	 * method for adding root
	 * 
	 * @param root
	 */
	public void addroot(Node root) {
		if (root != null) {
			LOGGER.info("root need to be added is not null");
			hmtree.put(root.getValue(), root);
			LOGGER.info("root node is added");
			LOGGER.info("tree "+hmtree);
		}
	}

	/**
	 * method for inserting node
	 * 
	 * @param parent
	 * @param node
	 * @param dir
	 */
	public void addnode(String dir, Node parent, Node node) {
		if ("right".equals(dir)) {
			parent.right = node;
		} else {
			parent.left = node;
		}
		hmtree.put(node.getValue(), node);
		LOGGER.info("tree "+hmtree);

	}
	/**
	 * depth first search method for tree traversal
	 * @param node
	 */

	public void dfs(Node node) {
		if (node != null) {
			LOGGER.info("value "+node.getValue());
		}
		if (node != null && node.left != null) {
			dfs(node.left);
		}
		if (node != null && node.right != null) {
			dfs(node.right);
		}
	}
	public int getsize()
	{
		return hmtree.size();
	}

}