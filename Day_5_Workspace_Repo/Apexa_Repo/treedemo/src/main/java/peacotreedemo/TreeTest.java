package peacotreedemo;

import static org.junit.Assert.*;

import org.junit.Test;

public class TreeTest {

	@Test
	public void testAddroot() {
		Tree tree = new Tree();
		Node root = new Node(4);
		tree.addroot(root);
		assertEquals(root.getValue(), 4);
		assertEquals(1, tree.getsize());
	}

	@Test
	public void testAddnode() {
		Tree tree = new Tree();
		Node root = new Node(3);
		tree.addroot(root);
		Node node= new Node(6);
		tree.addnode("left", root, node);
		Node node1= new Node(7);
		tree.addnode("right",root, node1);
		Node node2= new Node(16);
		tree.addnode("right", node, node2);
		Node node3= new Node(12);
		tree.addnode("left", node, node3);
		assertEquals(5, tree.getsize());
	}


}
