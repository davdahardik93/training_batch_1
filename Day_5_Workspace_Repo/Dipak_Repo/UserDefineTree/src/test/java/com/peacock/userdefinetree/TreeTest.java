package com.peacock.userdefinetree;

import junit.framework.TestCase;
public class TreeTest extends TestCase{
	
	public TreeTest(){
		super();
	}
	
	TreeDemo tree = new TreeDemo();
	
	public void testRoot() {
		tree.setRoot(212);
		assertEquals(212, tree.nodevalue);
	}
	
	public void testfailRoot(){
		tree.setRoot(23);
		assertNotSame(2, tree.nodevalue);
	}
	
	public void testChildNode() {
		tree.setRoot(20);
		tree.createchildNode(32, 20, 'l');
		tree.createchildNode(50, 32, 'r');
		assertEquals(3, tree.treeSize());
	}
	
	public void testDFS() {
		tree.setRoot(20);
		assertEquals(20, tree.nodevalue);
		tree.createchildNode(30, 20, 'l');
		tree.createchildNode(50, 20, 'r');
		tree.createchildNode(60, 50, 'r');
		tree.createchildNode(70, 30, 'l');
		tree.createchildNode(80, 50, 'l');
		tree.createchildNode(90, 30, 'r');
		tree.createchildNode(100, 70, 'l');
		tree.createchildNode(110, 100, 'l');
		tree.createchildNode(40, 80, 'r');
		assertEquals(10, tree.treeSize());
		tree.depthSearch();
		tree.getElements();
	}

	public void testBFS() {
		tree.setRoot(20);
		assertEquals(20, tree.nodevalue);
		tree.createchildNode(30, 20, 'l');
		tree.createchildNode(50, 20, 'r');
		tree.createchildNode(60, 50, 'r');
		tree.createchildNode(70, 30, 'l');
		tree.createchildNode(80, 50, 'l');
		tree.createchildNode(90, 30, 'r');
		tree.createchildNode(100, 70, 'l');
		tree.createchildNode(110, 100, 'l');
		tree.createchildNode(40, 80, 'r');
		assertEquals(10, tree.treeSize());
		tree.breadthSearch();
		tree.getElements();
		
	}

}
