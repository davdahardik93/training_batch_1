package com.peacock.userdefinetree;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.logging.Logger;

public class TreeDemo {
	Node root;
	Node child;
	int nodevalue;
	private static final Logger LOGGER = Logger.getLogger(TreeDemo.class.getName());
	Map<Integer, Node> tree = new HashMap<Integer, Node>();
	
	void setRoot(int data){
		
		root = new Node();
		root.setValue(data);
		root.setLeft(null);
		root.setRight(null);
		
		nodevalue = data;
		tree.put(root.getValue(), root);
	}
	
	void createchildNode(int data, int pvalue, char side){
		child = new Node();
		
		child.setValue(data);
		child.setLeft(null);
		child.setRight(null);
		tree.put(child.getValue(), child);
		
		setParent(child,pvalue,side);
	}
	
	void setParent(Node child, int pvalue, char dir){
		if(dir == 'r'){
			Node parent = tree.get(pvalue);
			if(parent.getRight() == null){
				parent.setRight(child);
			} else {
				LOGGER.info("This position is not available!!");
			}
		}else if(dir == 'l'){
			Node parent = tree.get(pvalue);
			if (parent.getRight() == null) {
				parent.setRight(child);
			} else {
				LOGGER.info("This position is not availale!!");
			}
		}else{
			LOGGER.info("Not Valid!!");
		}
	}
	
	public void depthSearch() {
		dfs(root);
	}

	public void dfs(Node root) {
		if (root != null) {
			LOGGER.info("value : " + root.getValue());
		}
		if (root != null && root.getLeft() != null) {
			dfs(root.getLeft());
		}
		if (root != null && root.getRight() != null) {
			dfs(root.getRight());
		}
	}


	public void breadthSearch() {
		bfs(root);
	}

	public void bfs(Node root) {
		Queue<Node> queue = new LinkedList<Node>();
		queue.add(root);
		bfsRec(queue);
	}


	public void bfsRec(Queue<Node> q) {
		if (q.isEmpty()) {
			return;
		}
		Node chilNode = q.poll();
		if (chilNode != null) {
			LOGGER.info("value : " + chilNode.getValue());
		}
		if (chilNode != null && chilNode.getLeft() != null) {
			q.add(chilNode.getLeft());
		}
		if (chilNode != null && chilNode.getRight() != null) {
			q.add(chilNode.getRight());
		}
		bfsRec(q);
	}

	public void getElements() {
		for (Integer key : tree.keySet()) {
			LOGGER.info("keys are :" + key);
		}
	}

	public int treeSize() {
		return tree.size();
	}
}
