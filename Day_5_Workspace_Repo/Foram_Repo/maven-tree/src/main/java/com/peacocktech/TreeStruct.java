package com.peacocktech;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.logging.Logger;

public class TreeStruct {
	TreeNode root;
	int rootValue;

	private static final Logger LOGGER = Logger.getLogger(TreeStruct.class.getName());

	Map<Integer, TreeNode> store = new HashMap<Integer, TreeNode>();

	public void setRoot(int value) {
		TreeNode root = new TreeNode(value, null, null);
		store.put(root.value, root);
		rootValue = value;

	}

	public void createChild(int value, int pValue, char dir) {
		TreeNode child = new TreeNode(value, null, null);
		store.put(value, child);
		setParent(child, pValue, dir);
	}

	public void setParent(TreeNode child, int pValue, char dir) {
		TreeNode parent = new TreeNode(pValue, null, null);
		parent = store.get(pValue);
		if (dir == 'r') {
			parent.setRight(child);
		}
		if (dir == 'l') {
			parent.setLeft(child);
		}
	}

	public void depthSearch() {
		dfs(root);
	}

	public void dfs(TreeNode root) {
		if (root != null) {
			LOGGER.info("value : " + root.getValue());
		}
		if (root != null && root.getLeft() != null) {
			dfs(root.getLeft());
		}
		if (root != null && root.getRight() != null) {
			dfs(root.getRight());
		}
	}

	public void breadthSearch() {
		bfs(root);
	}

	public void bfs(TreeNode root) {
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		queue.add(root);
		bfsRec(queue);
	}

	public void bfsRec(Queue<TreeNode> q) {
		if (q.isEmpty()) {
			return;
		}
		TreeNode childNode = q.poll();
		if (childNode != null) {
			LOGGER.info("value : " + childNode.getValue());
		}
		if (childNode != null && childNode.getLeft() != null) {
			q.add(childNode.getLeft());
		}
		if (childNode != null && childNode.getRight() != null) {
			q.add(childNode.getRight());
		}
		bfsRec(q);
	}

	public void getElements() {
		for (Integer key : store.keySet()) {
			LOGGER.info("keys are :" + key);
		}
	}

	public int sizeOfTree() {
		return store.size();
	}

}
