package com.peacocktech;

public class TreeNode {
	int value;
	TreeNode left, right;

	public TreeNode(int value, TreeNode left, TreeNode right) {

		this.value = value;
		this.left = null;
		this.right = null;
	}

	public void setLeft(TreeNode node) {
		this.left = left;
	}

	public void setRight(TreeNode node) {
		this.right = right;
	}

	public TreeNode getLeft() {
		return left;
	}

	public TreeNode getRight() {
		return right;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
