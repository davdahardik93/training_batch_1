package com.peacocktech;
import junit.framework.TestCase;

public class TreeTest extends TestCase{
	
	TreeStruct tree = new TreeStruct();

	public void testRoot() {
		tree.setRoot(20);
		assertEquals(20, tree.rootValue);
	}

	public void testChildNode() {
		tree.setRoot(20);
		tree.createChild(30, 20, 'l');
		tree.createChild(50, 20, 'r');
		assertEquals(3, tree.sizeOfTree());
		tree.getElements();// This will print all the elements of tree in
							// console
	}

	public void testDFS() {
		tree.setRoot(20);
		assertEquals(20, tree.rootValue);
		tree.createChild(30, 20, 'l');
		tree.createChild(50, 20, 'r');
		tree.createChild(60, 50, 'r');
		tree.createChild(70, 30, 'l');
		tree.createChild(80, 50, 'l');
		tree.createChild(90, 30, 'r');
		tree.createChild(100, 70, 'l');
		tree.createChild(110, 100, 'l');
		tree.createChild(40, 80, 'r');
		assertEquals(10, tree.sizeOfTree());
		tree.depthSearch();
		tree.getElements();// This will print all the elements of tree in
		// console

	}

	public void testBFS() {
		tree.setRoot(20);
		assertEquals(20, tree.rootValue);
		tree.createChild(30, 20, 'l');
		tree.createChild(50, 20, 'r');
		tree.createChild(60, 50, 'r');
		tree.createChild(70, 30, 'l');
		tree.createChild(80, 50, 'l');
		tree.createChild(90, 30, 'r');
		tree.createChild(100, 70, 'l');
		tree.createChild(110, 100, 'l');
		tree.createChild(40, 80, 'r');
		assertEquals(10, tree.sizeOfTree());
		tree.breadthSearch();
		tree.getElements();// This will print all the elements of tree in
		// console
	}

}
