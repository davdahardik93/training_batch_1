package com.peacocktech;

import java.util.*;
import java.util.logging.Logger;

/**
 * 
 * @author Peacock
 *
 */
class Node {
	int value;
	Node left;
	Node right;

	public Node(int value, Node left, Node right) {
		this.value = value;
		this.left = left;
		this.right = right;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public Node getLeft() {
		return left;
	}

	public void setLeft(Node left) {
		this.left = left;
	}

	public Node getRight() {
		return right;
	}

	public void setRight(Node right) {
		this.right = right;
	}
}

/**
 * 
 * @author Peacock
 *
 */
public class Tree {
	Node root;
	Node node;
	int rootValue;
	private static final Logger log = Logger.getLogger(Tree.class.getName());

	Map<Integer, Node> treeMap = new HashMap<>();

	/**
	 * It sets the root of the tree
	 * 
	 * @param input - This will be root element
	 *            
	 */
	public void setRoot(int input) {
		root = new Node(input, null, null);
		treeMap.put(input, root);
		log.info("Size after addding root:" + treeMap.size());
		rootValue = input;
	}

	/**
	 * It will insert the node in tree
	 * 
	 * @param input - Value of node
	 *           
	 * @param parentValue - This will specify the parent of the newly inserted node
	 *            
	 * @param dir - Specifies left or right with respect to parentValue
	 *           
	 */
	public void insert(int input, int parentValue, char dir) {
		node = new Node(input, null, null);
		treeMap.put(input, node);
		setLink(node, parentValue, dir);
	}

	/**
	 * Sets link between parent and child node
	 * 
	 * @param node - Object of child
	 *           
	 * @param parentValue - Parent of the current node
	 *            
	 * @param dir - Specifies direction of the child node
	 *            
	 */
	public void setLink(Node node, int parentValue, char dir) {
		if (dir == 'l') {
			Node parent = treeMap.get(parentValue);
			if (parent.getLeft() == null) {
				parent.setLeft(node);
			} else {
				log.info("Node already exists at this position");
			}
		}
		if (dir == 'r') {
			Node parent = treeMap.get(parentValue);
			if (parent.getRight() == null) {
				parent.setRight(node);
			} else {
				log.info("Node already exists at this position");
			}
		}
	}

	/**
	 * Depth First Search Traversal
	 */
	public void depthSearch() {
		dfs(root);
	}

	/**
	 * It starts the DFS from root node
	 * 
	 * @param root
	 *            
	 */
	public void dfs(Node root) {
		if (root != null) {
			log.info("Value : " + root.getValue());
		}
		if (root != null && root.getLeft() != null) {
			dfs(root.getLeft());
		}
		if (root != null && root.getRight() != null) {
			dfs(root.getRight());
		}
	}

	/**
	 * Breadth First Search traversal
	 */
	public void breadthSearch() {
		bfs(root);
	}

	/**
	 * It is used to add root node in queue
	 * 
	 * @param root
	 *            
	 */
	public void bfs(Node root) {
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		bfsRec(queue);
	}

	/**
	 * It starts BFS from root node
	 * 
	 * @param q - Contains node while traversing 
	 *           
	 */
	public void bfsRec(Queue<Node> q) {
		if (q.isEmpty()) {
			return;
		}
		Node childNode = q.poll();
		if (childNode != null) {
			log.info("Value : " + childNode.getValue());
		}
		if (childNode != null && childNode.getLeft() != null) {
			q.add(childNode.getLeft());
		}
		if (childNode != null && childNode.getRight() != null) {
			q.add(childNode.getRight());
		}
		bfsRec(q);
	}

	/**
	 * To get the elements of the Tree
	 */
	public void getElements() {
		for (Integer key : treeMap.keySet()) {
			log.info("Keys are :" + key);
		}
	}

	/**
	 * It returns the size of tree
	 * 
	 * @return Size of tree
	 */
	public int sizeOfTree() {
		return treeMap.size();
	}
}
