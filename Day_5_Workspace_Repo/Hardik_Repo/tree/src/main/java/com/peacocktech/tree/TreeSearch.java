package com.peacocktech.tree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 * 
 * @author peacock
 *
 */
public class TreeSearch {
/**
 * @param args
 */
	Node root;
	HashMap<Number,Node> map = new HashMap<Number,Node>();
	Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	ArrayList<Number> queue1 = new ArrayList<Number>();
	
	
	/**
	 * Add a Child to parent node
	 * In this direction is true means right side of the parent node and vice versa 
	 * @param parentValue
	 * @param childValue
	 * @param direction
	 */
	public void add(int parentValue, int childValue, Boolean direction)
	{
		if(root == null)
		{
			setroot(childValue);
			return;
		}
		
		Node parentNode = map.get(parentValue);
		if(parentNode ==null)
		{
			displayLog("parent node"+ parentValue+"is not available");
			return;			
		}
		
		if(map.get(childValue) != null){
			displayLog("child node"+ childValue+"is already available");
			return;
		}
		
		
		Node childNode = new Node(childValue);
		if(direction)
		{
			if(parentNode.getRight() == null)
			{
				parentNode.setRight(childNode);
				map.put(childValue,childNode);
				displayLog("node added:"+childValue+" parent node: " +parentValue+ " to the: right");
			}
			else
				displayLog("parent node "+ parentValue + "has already available right node");
		}
		else
		{
			if(parentNode.getLeft() == null)
			{
				parentNode.setLeft(childNode);
				map.put(childValue,childNode);
				displayLog("node added:"+childValue+" parent node: " +parentValue+ " to the: left");
			}
			else
				displayLog("parent node "+ parentValue + "has already available left node");
		}
		
	}
	/**
	 * traversal tree in Breadth First Search...
	 * @param value
	 */
	public void bfssearch(int value)
	{
		if(map.get(value) == null)
			return;
		ArrayList<Number> queue = new ArrayList<Number>();
		queue.add(value);
		int index = 0;
		Node node;
		while(index != queue.size())
		{
			node = map.get(queue.get(index));
			if(node.getLeft() != null)
				queue.add(node.getLeft().getValue());
			if(node.getRight() != null)
				queue.add(node.getRight().getValue());
			
			index++;
		}
		
		displayLog(queue+"");
		
	}
	
	/**
	 * traversal tree in Depth First Search...
	 * @param value
	 */
	public void dfssearch(int value)
	{
		if(map.get(value) == null)
			return;
		Node node = map.get(value);
		
		queue1.add(value);
		if(node.getLeft() != null)
			dfssearch(node.getLeft().getValue());
		if(node.getRight() != null)
			dfssearch(node.getRight().getValue());
		
		if(node == root)
			displayLog(queue1+"");
	}
	
	
	private void displayLog(String msg)
	{
		log.info(msg);
	}
	
	/**
	 * Set root of tree..
	 * @param value
	 */
	public void setroot(int value)
	{
		if(root == null)
		{
			root = new Node(value);
			map.put(value,root);
			displayLog("root node created");
		}
		else
		{
			displayLog("root node is already available");
		}
	}
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		TreeSearch tree = new TreeSearch();
		
		tree.setroot(1000);
		tree.add(1000, 500, true);
		tree.add(1000, 200, false);
		tree.add(500,4,false);
		tree.add(200,6888,true);
		tree.add(500,56,true);
		tree.bfssearch(1000);
		
		tree.add(6888,433,true);
		tree.dfssearch(1000);
		
	}
	/**
	 * this method for node is available or not
	 * @param value
	 * @return
	 */
	public boolean nodeavailable(int value)
	{
		if(map.get(value) != null)
			return true;
		return false;
	}
	/**
	 * this method gives total node in tree..
	 * @return
	 */
	public int gettotalnode()
	{
		return map.size();
	}
}
