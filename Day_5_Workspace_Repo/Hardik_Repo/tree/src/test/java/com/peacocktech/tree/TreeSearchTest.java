package com.peacocktech.tree;

import junit.framework.TestCase;

public class TreeSearchTest extends TestCase {

	
	TreeSearch tree = new TreeSearch();
	
	public void testaddroot()
	{
		tree.setroot(1000);
		
		assertEquals(1000,tree.root.getValue());
		
	}
	
	public void testupdateroot()
	{
		tree.setroot(1000);
		tree.setroot(5000);
		assertNotNull(tree.root);
		assertEquals(1000, tree.root.getValue());
	}
	
	public void testtotalnode()
	{
		//In this direction is true means right side of the parent node and vice versa 
		tree.setroot(1000);
		tree.add(1000,300,true);
		tree.add(300, 45, false);
		tree.add(1000, 56, false);
		assertEquals(4, tree.gettotalnode());
	}
	
	public void testnodeavailablecheck()
	{
		tree.setroot(1000);
		tree.add(1000, 599, true);
		tree.add(55, 9090, false);
		tree.add(599, 23, true);
		assertNotSame(true,tree.nodeavailable(9090));
	}
	
	public void testnodeavailable()
	{
		tree.setroot(1000);
		tree.add(1000, 599, true);
		tree.add(55, 9090, false);
		tree.add(599, 23, true);
		assertSame(true,tree.nodeavailable(23));
	}
	//test node: add same position..
	public void testaddnnode()
	{
		tree.setroot(1000);
		tree.add(1000,300,true);
		tree.add(300, 45, false);
		tree.add(1000, 56, true);//add same position of 300
		assertNotSame(56, tree.nodeavailable(56));
	}
	
	public void testbfstravasal()
	{
		tree.setroot(1000);
		tree.add(1000,300,true);
		tree.add(300, 45, false);
		tree.add(1000, 56, false);
		tree.add(45, 95,true);
		tree.bfssearch(1000);//output is available on console
	}
	
	public void testdfstravasal()
	{
		tree.setroot(1000);
		tree.add(1000,300,true);
		tree.add(300, 45, false);
		tree.add(1000, 56, false);
		tree.add(45, 95,true);
		tree.dfssearch(1000);//output is available on console
	}
	
	//add node in tree which is already available
	public void testaddnodeavailble()
	{
		tree.setroot(1000);
		tree.add(1000,300,true);
		tree.add(300, 45, false);
		tree.add(1000, 56, false);
		tree.add(45, 95,true);
		tree.add(45,300, true);
		tree.bfssearch(1000);
	}
}
