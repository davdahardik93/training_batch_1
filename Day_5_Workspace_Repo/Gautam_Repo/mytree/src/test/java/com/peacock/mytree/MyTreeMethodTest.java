package com.peacock.mytree;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple Tree method.
 */
public class MyTreeMethodTest extends TestCase {
	MyTreeMethod treeMethod;

	/**
	 * Create the test case
	 *
	 * @param testName
	 *            name of the test case
	 */
	public MyTreeMethodTest(String testName) {
		super(testName);
		treeMethod = new MyTreeMethod();
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(MyTreeMethodTest.class);
	}

	/**
	 * for set root node testing
	 */
	public void testSetNodeForRoot() {
		treeMethod.setNode(true, 100, 10);
		// treeMethod.setNode(false, 10, 100);
		assertEquals(1, treeMethod.getSize());
	}

	/**
	 * for fail set root node testing
	 */
	public void testFailSetNodeForRoot() {
		treeMethod.setNode(true, 100, 10);
		treeMethod.setNode(true, 10, 10);
		// treeMethod.setNode(false, 10, 100);
		assertNotSame(0, treeMethod.getSize());
	}

	/**
	 * for set node testing for parent not found
	 */
	public void testSetNode() {
		treeMethod.setNode(true, 100, 10);
		treeMethod.setNode(true, 10, 10);
		// treeMethod.setNode(false, 10, 100);
		assertEquals(1, treeMethod.getSize());
	}

	/**
	 * for fail set node testing for parent not found
	 */
	public void testFailSetNode() {
		treeMethod.setNode(true, 100, 10);
		treeMethod.setNode(true, 10, 10);
		// treeMethod.setNode(false, 10, 100);
		assertNotSame(2, treeMethod.getSize());
	}

	/**
	 * for set node testing
	 */
	public void testSetNodeD() {
		treeMethod.setNode(true, 100, 10);
		treeMethod.setNode(true, 100, 100);
		// treeMethod.setNode(false, 10, 100);
		assertEquals(1, treeMethod.getSize());
	}

	/**
	 * for fail set node testing for parent not found
	 */
	public void testFailSetNodeD() {
		treeMethod.setNode(true, 100, 10);
		treeMethod.setNode(true, 100, 100);
		// treeMethod.setNode(false, 10, 100);
		assertNotSame(2, treeMethod.getSize());
	}

	/**
	 * for dfs traverse
	 */
	public void testDfsTraverse() {
		genrateTree();
		treeMethod.setCounter(0);
		treeMethod.depFthFirstTraverse(100);
		assertEquals(6, treeMethod.getCounter());
	}

	/**
	 * for fail dfs traverse
	 */
	public void testFailDfsTraverse() {
		genrateTree();
		treeMethod.setCounter(0);
		treeMethod.depFthFirstTraverse(100);
		assertNotSame(5, treeMethod.getCounter());
	}

	/*-------------100
	 *-----------/     \
	 *---------10       20
	 *--------/  \        \
	 *------110  111       30
	 */
	private void genrateTree() {
		treeMethod.setNode(true, 100, 10);
		treeMethod.setNode(true, 10, 100);
		treeMethod.setNode(false, 20, 100);
		treeMethod.setNode(false, 30, 20);
		treeMethod.setNode(false, 110, 10);
		treeMethod.setNode(true, 111, 10);
	}

	/**
	 * for bfs traverse
	 */
	public void testBfsTraverse() {
		genrateTree();
		treeMethod.setCounter(0);
		treeMethod.breathFirstTraverse();
		assertEquals(6, treeMethod.getCounter());
	}

	/**
	 * for fail bfs traverse
	 */
	public void testFailBfsTraverse() {

		genrateTree();
		treeMethod.setCounter(0);
		treeMethod.breathFirstTraverse();
		assertNotSame(5, treeMethod.getCounter());
	}
}
