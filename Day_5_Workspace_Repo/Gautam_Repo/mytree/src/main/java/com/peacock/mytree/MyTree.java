package com.peacock.mytree;

/**
 * Tree structure
 * 
 * @author peacock
 *
 */
public class MyTree {
	private int value;
	private int rightPos;
	private int leftPos;
	private int parenPos;

	/**
	 * init value of node and set positions with -1
	 * 
	 * @param value
	 */
	public MyTree(int value) {
		this.value = value;
		this.parenPos = -1;
		this.leftPos = -1;
		this.rightPos = -1;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getRightPos() {
		return rightPos;
	}

	public void setRightPos(int rightPos) {
		this.rightPos = rightPos;
	}

	public int getLeftPos() {
		return leftPos;
	}

	public void setLeftPos(int leftPos) {
		this.leftPos = leftPos;
	}

	public int getParenPos() {
		return parenPos;
	}

	public void setParenPos(int parenPos) {
		this.parenPos = parenPos;
	}

}
