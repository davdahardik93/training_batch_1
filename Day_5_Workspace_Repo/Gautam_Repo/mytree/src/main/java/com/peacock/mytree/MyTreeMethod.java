package com.peacock.mytree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Class for method of My Tree class
 * 
 * @author peacock
 *
 */
public class MyTreeMethod {
	public static final String TAG = MyTreeMethod.class.getName();
	private HashMap<String, MyTree> hashMap;
	private boolean isRootSet = false;
	int root = -1;

	private int counter = 0;

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	/**
	 * Generate new node and set in the tree
	 * 
	 * @param isLeft
	 * @param valueOfNewNode
	 * @param valueOfParentNode
	 */
	public void setNode(boolean isLeft, int valueOfNewNode, int valueOfParentNode) {
		if (isRootSet) {// if root is set then set next node else set root 1st
			printLog("hashmap---" + hashMap);
			if (hashMap.containsKey(valueOfNewNode)) {
				printLog("enter other value than-" + valueOfNewNode);
				return;
			} // new node is duplicate
				///////////////////////////////////////////////////////////////////////////////
			MyTree treeNode = new MyTree(valueOfNewNode);
			MyTree treeParentNode = hashMap.get(toString(valueOfParentNode));

			if (treeParentNode != null) {
				treeNode.setParenPos(treeParentNode.getValue());
				if (isLeft) {
					treeParentNode.setLeftPos(valueOfNewNode);
				} else {
					treeParentNode.setRightPos(valueOfNewNode);
				}
			} // not null
			else {
				printLog("no parent found!!..enter other value than-" + valueOfParentNode);
				return;
			}
			hashMap.put(toString(valueOfNewNode), treeNode);
			printLog("node genrated with value-" + hashMap.get(toString(valueOfNewNode)));

		} else {
			setRootNode(valueOfNewNode);
		} // set root node
	}

	/**
	 * set Root Node
	 * 
	 * @param value
	 */
	private void setRootNode(int value) {
		hashMap = new HashMap<String, MyTree>();
		MyTree tree = new MyTree(value);
		hashMap.put(toString(value), tree);
		isRootSet = true;
		printLog("root genrated with value-" + value);
		root = value;
	}

	/**
	 * dfs traverse using stack
	 * 
	 * @param value
	 *            give root node value
	 */
	public void depFthFirstTraverse(int value) {
		MyTree tree = hashMap.get(toString(value));
		if (tree == null) {
			return;
		} // break condition
		counter++;
		printLog(toString(tree.getValue()));
		depFthFirstTraverse(tree.getLeftPos());
		depFthFirstTraverse(tree.getRightPos());
	}// depth first search

	/**
	 * bfs traverse using queue
	 * 
	 * @return return number of total node
	 */
	public void breathFirstTraverse() {
		ArrayList<String> arrayList = new ArrayList<String>();
		arrayList.add(toString(root));// init with root
		int temp;
		MyTree tree;
		for (counter = 0; counter < hashMap.size(); counter++) {
			tree = hashMap.get(arrayList.get(counter));
			if (tree != null) {
				printLog(toString(tree.getValue()));
				temp = tree.getLeftPos();
				if (temp != -1) {
					arrayList.add(toString(hashMap.get(toString(temp)).getValue()));
				} // for left pos
				temp = tree.getRightPos();
				if (temp != -1) {
					arrayList.add(toString(hashMap.get(toString(temp)).getValue()));
				} // for right pos
			}
		} // for
	}

	/**
	 * method to convert integer to String using Integer.toString(int value)
	 * 
	 * @param value
	 * @return String object of given int
	 * 
	 */
	private String toString(int value) {
		return Integer.toString(value);
	}

	/**
	 * method will print info log in console with given message
	 * 
	 * @param msg
	 */
	private void printLog(String msg) {
		Logger logger = Logger.getLogger(TAG);
		logger.info(msg);
	}

	/**
	 * return size of the hash map
	 */
	public int getSize() {
		return hashMap.size();
	}
}// classf
