import junit.framework.TestCase;

public class TreeMainTest extends TestCase{

	  
		TreeMain tree = new TreeMain();

		public void testRoot() {
			tree.setRoot(10);
			assertEquals(10, tree.rootValue);
		}

		public void testChildNode() {
			tree.setRoot(22);
			tree.createNode(30, 22, 'l');
			tree.createNode(50, 22, 'r');
			assertEquals(3, tree.sizeOfTree());
			tree.getElements();
		}

		public void testDFS() {
			tree.setRoot(21);
			assertEquals(21, tree.rootValue);
			tree.createNode(30, 21, 'l');
			tree.createNode(50, 21, 'r');
			tree.createNode(60, 50, 'r');
			tree.createNode(70, 30, 'l');
			tree.createNode(80, 50, 'l');
			tree.createNode(90, 30, 'r');
			tree.createNode(100, 70, 'l');
			tree.createNode(110, 100, 'l');
			tree.createNode(40, 80, 'r');
			assertEquals(10, tree.sizeOfTree());
			tree.depthSearch();
			tree.getElements();
		}

		public void testBFS() {
			tree.setRoot(23);
			assertEquals(23, tree.rootValue);
			tree.createNode(30, 23, 'l');
			tree.createNode(50, 23, 'r');
			tree.createNode(60, 50, 'r');
			tree.createNode(70, 30, 'l');
			tree.createNode(80, 50, 'l');
			tree.createNode(90, 30, 'r');
			tree.createNode(100, 70, 'l');
			tree.createNode(110, 100, 'l');
			tree.createNode(40, 80, 'r');
			assertEquals(10, tree.sizeOfTree());
			tree.breadthSearch();
			tree.getElements();
		}
	}

	
	

