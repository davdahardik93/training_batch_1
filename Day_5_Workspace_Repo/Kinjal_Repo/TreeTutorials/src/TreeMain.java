import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.logging.Logger;

 class TreeMain{

	Node root;
	Node node;
	int rootValue;
	private static final Logger LOGGER = Logger.getLogger(TreeMain.class.getName());

	Map<Integer, Node> treeMap = new HashMap<>();

	/**
	 * add root node in tree
	 * 
	 * @param input
	 *            
	 */
	public void setRoot(int input) {
		root = new Node(input, null, null);
		treeMap.put(input, root);
		LOGGER.info("Size after addding root:" + treeMap.size());
		rootValue = input;
	}

	/**
	 * It creates the node of tree
	 * 
	 * @param input
	 *            -value for childnode
	 * @param parentValue
	 *            - Value of parent
	 * @param dir
	 *            - Direction(left/right) 
	 */
	public void createNode(int input, int parentValue, char dir) {
		node = new Node(input, null, null);
		treeMap.put(input, node);
		setLink(node, parentValue, dir);
	}

	/**
	 * Sets link between parent and child node
	 * 
	 * @param node
	 *            - Object of child node
	 * @param parentValue
	 *            - Value of parent
	 * @param dir
	 *            - Direction (left/right)
	 */
	public void setLink(Node node, int parentValue, char dir) {
		if (dir == 'l') {
			Node parent = treeMap.get(parentValue);
			if (parent.getLeft() == null) {
				parent.setLeft(node);
			} else {
				LOGGER.info("Node is already available at this position");
			}
		}
		if (dir == 'r') {
			Node parent = treeMap.get(parentValue);
			if (parent.getRight() == null) {
				parent.setRight(node);
			} else {
				LOGGER.info("Node is already available at this position");
			}
		}
	}

	/**
	 * It is used for Depth First Search traversal
	 */
	public void depthSearch() {
		dfs(root);
	}

	/**
	 * It starts the DFS from root node
	 * 
	 * @param root
	 *            - root node
	 */
	public void dfs(Node root) {
		if (root != null) {
			LOGGER.info("value : " + root.getValue());
		}
		if (root != null && root.getLeft() != null) {
			dfs(root.getLeft());
		}
		if (root != null && root.getRight() != null) {
			dfs(root.getRight());
		}
	}

	/**
	 *  Breadth First Search traversal
	 */
	public void breadthSearch() {
		bfs(root);
	}

	/**
	 * add root node in queue
	 * 
	 * @param root
	 *            - root node
	 */
	public void bfs(Node root) {
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		bfsRec(queue);
	}

	/**
	 * It starts BFS from root node
	 * 
	 * @param q
	 *            - Queue which will contain nodes while traversing
	 */
	public void bfsRec(Queue<Node> q) {
		if (q.isEmpty()) {
			return;
		}
		Node chilNode = q.poll();
		if (chilNode != null) {
			LOGGER.info("value : " + chilNode.getValue());
		}
		if (chilNode != null && chilNode.getLeft() != null) {
			q.add(chilNode.getLeft());
		}
		if (chilNode != null && chilNode.getRight() != null) {
			q.add(chilNode.getRight());
		}
		bfsRec(q);
	}

	/**
	 * prints all the elements presented in tree
	 */
	public void getElements() {
		for (Integer key : treeMap.keySet()) {
			LOGGER.info("keys are :" + key);
		}
	}

	/**
	 * size of tree
	 * 
	 * @return size of tree
	 */
	public int sizeOfTree() {
		return treeMap.size();
	}
}


