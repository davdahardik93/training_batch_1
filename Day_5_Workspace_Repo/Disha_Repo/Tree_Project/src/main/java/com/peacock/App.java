package com.peacock;

import java.util.*;
import java.util.logging.Logger;

/**
 * 
 * 
 * @author Disha
 *
 */
class Node {
	int value;
	Node left;
	Node right;

	public Node(int value, Node left, Node right) {
		super();
		this.value = value;
		this.left = left;
		this.right = right;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public Node getLeft() {
		return left;
	}

	public void setLeft(Node left) {
		this.left = left;
	}

	public Node getRight() {
		return right;
	}

	public void setRight(Node right) {
		this.right = right;
	}
}

/**
 * 
 * 
 * @author Disha
 *
 */
public class App {
	Node root;
	Node node;
	int rootValue;
	private static final Logger LOGGER = Logger.getLogger(App.class.getName());

	Map<Integer, Node> treeMap = new HashMap<>();

	/**
	 * It sets the root element in tree
	 * 
	 * @param input
	 *            - Value which becomes root of the tree
	 */
	public void setRoot(int input) {
		root = new Node(input, null, null);
		treeMap.put(input, root);
		LOGGER.info("Size after addding root:" + treeMap.size());
		rootValue = input;
	}

	/**
	 * It creates the node of tree
	 * 
	 * @param input
	 *            
	 * @param parentValue
	 *            
	 * @param dir
	 *            
	 */
	public void createNode(int input, int parentValue, char dir) {
		node = new Node(input, null, null);
		treeMap.put(input, node);
		setLink(node, parentValue, dir);
	}

	/**
	 * 
	 * 
	 * @param node
	 * 
	 * @param parentValue
	 * 
	 * @param dir
	 * 
	 */
	public void setLink(Node node, int parentValue, char dir) {
		if (dir == 'l') {
			Node parent = treeMap.get(parentValue);
			if (parent.getLeft() == null) {
				parent.setLeft(node);
			} else {
				LOGGER.info("Node is already available at this position");
			}
		}
		if (dir == 'r') {
			Node parent = treeMap.get(parentValue);
			if (parent.getRight() == null) {
				parent.setRight(node);
			} else {
				LOGGER.info("Node is already available at this position");
			}
		}
	}

	/**
	 * It is used for Depth First Search traversal
	 */
	public void depthSearch() {
		dfs(root);
	}

	/**
	 * It starts the DFS from root node
	 * 
	 * @param root
	 * 
	 */
	public void dfs(Node root) {
		if (root != null) {
			LOGGER.info("value : " + root.getValue());
		}
		if (root != null && root.getLeft() != null) {
			dfs(root.getLeft());
		}
		if (root != null && root.getRight() != null) {
			dfs(root.getRight());
		}
	}

	/**
	 * It is used for Breadth First Search traversal
	 */
	public void breadthSearch() {
		bfs(root);
	}

	/**
	 * 
	 * 
	 * @param root
	 * 
	 */
	public void bfs(Node root) {
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		bfsRec(queue);
	}

	/**
	 * 
	 * 
	 * @param q
	 * 
	 */
	public void bfsRec(Queue<Node> q) {
		if (q.isEmpty()) {
			return;
		}
		Node chilNode = q.poll();
		if (chilNode != null) {
			LOGGER.info("value : " + chilNode.getValue());
		}
		if (chilNode != null && chilNode.getLeft() != null) {
			q.add(chilNode.getLeft());
		}
		if (chilNode != null && chilNode.getRight() != null) {
			q.add(chilNode.getRight());
		}
		bfsRec(q);
	}

	/**
	 * It prints all the elements presented in tree
	 */
	public void getElements() {
		for (Integer key : treeMap.keySet()) {
			LOGGER.info("keys are :" + key);
		}
	}

	/**
	 * 
	 * 
	 * @return size of tree
	 */
	public int sizeOfTree() {
		return treeMap.size();
	}
}
