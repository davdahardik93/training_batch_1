# README #

This repositories for the Surat base training batch 01 and for uploading their assignments.

### What is this repository for? ###

* Daily Assignments of each trainee
* Version 1.0


### Contribution guidelines ###

* Understand the problem statement 
* Write the code for the problem statement
* Use all necessary plugins for code review and code organization
* Submit your code

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact